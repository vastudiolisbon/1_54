��    (      \  5   �      p     q     y  0     -   �  0   �               -     A     R  &   ^  ?   �     �  =   �          -     ?     F     I     U     Z     z     �     �     �     �      �     �  =   �             	   &     0  *   C     n     s     v          �  '  �     �     �  0   �  -   �  0   #     T     a     r     �     �  &   �  ?   �     
	  =   	     [	     r	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	      �	     
  =   "
     `
     e
  	   k
     u
  +   �
     �
     �
     �
     �
     �
                                           $   !                 (       	   #         
                  &                         "      '                    %                          (from)  (to) <em>%s</em> is not a registered connection type. A list of posts connected to the current post Are you sure you want to delete all connections? Connected %s Connection Types Connection listing: Connection type: Connections Convert to registered connection type: Converted %1$s connections from <em>%2$s</em> to <em>%3$s</em>. Create connections Create many-to-many relationships between all types of posts. Delete all connections Delete connection Filter Go Information Name No connection types registered. No users found. Posts 2 Posts Search Search Users Settings Settings <strong>saved</strong>. Title: To register a connection type, see <a href="%s">the wiki</a>. User Users connected http://scribu.net/ http://scribu.net/wordpress/posts-to-posts next of previous related scribu PO-Revision-Date: 2018-10-16 23:55:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: en_GB
Project-Id-Version: Plugins - Posts 2 Posts - Stable (latest release)
  (from)  (to) <em>%s</em> is not a registered connection type. A list of posts connected to the current post Are you sure you want to delete all connections? Connected %s Connection Types Connection listing: Connection type: Connections Convert to registered connection type: Converted %1$s connections from <em>%2$s</em> to <em>%3$s</em>. Create connections Create many to many relationships between all types of posts. Delete all connections Delete connection Filter Go Information Name No connection types registered. No users found. Posts 2 Posts Search Search Users Settings Settings <strong>saved</strong>. Title: To register a connection type, see <a href="%s">the wiki</a>. User Users connected https://scribu.net/ https://scribu.net/wordpress/posts-to-posts next of previous related scribu 