<?php
/**
 * Prev & Next posts
 *
 * Used on all single news pages
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<div class="wrapper clear">
	<nav id="nav-below" class="navigation large-text" role="navigation">

		<div class="wrapper align-center">
			<div class="nav-previous">
				<?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> Past news' ); ?>&nbsp;
			</div>

			<div class="nav-next">
				<?php next_post_link( '%link', 'Recent news <span class="meta-nav">&rarr;</span>' ); ?>&nbsp;
			</div>
		</div>

	</nav>
</div>