<?php
/**
 * Single Exhibitor Page
 *
 * @package WordPress
 * @subpackage 1:54
 */

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

$off_page_bg = 'white-bg'; 

include(locate_template('header.php' )); 

// set defaults
$off_page_bg         = 'white-bg';
$module_bg           = '';

$terms           = '';
$edition_exists  = false;

$include_archive_nav = false;

$terms = get_the_terms($post->ID, 'edition');

// if page is tagged with the current edition, or has no edition at all ...
if ( ( get_query_var('edition') === '' || has_term( $off_current_edition, 'edition' ) ) || (array) $terms !== $terms ) {

	// do nothing

} else {
// page is tagged with an archived edition

	// check if we're already browsing a particular edition
	if ( get_query_var('edition') ) {
		// only show content from that edition
		$off_archive_edition  = get_query_var('edition');
		
		$module_bg       = 'light-bg edition-'.$off_archive_edition;
		// use the archive color for the whole page
		$off_page_bg     = $module_bg;
		// display the archive nav
		$include_archive_nav = true;

	}

	// check query var against the editions associated with this page
	if ( $terms && !is_wp_error($terms) ) {

		foreach ($terms as $term) {
			if ( $off_archive_edition == $term->slug ) {
				$edition_exists = true;
				break;
			}
		}
		// if the queried edition doesn't exist ... 
		if ( $edition_exists === false ) {
			// use the most recent edition
			$most_recent_term = $terms[count($terms)-1];

			$off_archive_edition  = $most_recent_term->slug;
			$module_bg       = 'light-bg edition-'.$off_archive_edition;
			// use the archive color for the whole page
			$off_page_bg     = $module_bg;
			// display the archive nav
			$include_archive_nav = true;
		}
	}
}

?>

<?php if ( $include_archive_nav === true ) include(locate_template('nav-above-archive.php' )); ?>


<?php 
	$activateLinkClass = '';

	if(is_singular('off_artist')) {
		$activateLinkClass = 'activate-artists-link';
	}
	else if(is_singular('off_exhibitor')) {
		$activateLinkClass = 'activate-exhibitors-link';
	}
?>


<section id="content" role="main" class="<?php echo $activateLinkClass; ?>">

	<?php get_template_part( 'entry','page-exhibitors' ); ?>

</section>

<?php include(locate_template('module-exhibitors-selected.php' )); ?>

<?php include(locate_template('module-exhibitors-list.php' )); ?>

<?php include(locate_template('footer.php' )); ?>