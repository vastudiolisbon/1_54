<?php
/**
 * Header Nav
 *
 * Appears on all pages, has two styles contingent upon options page selection: dark home page or light home page.
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<header id="header" class="page-row <?php if ( $off_page_bg === 'dark-bg' ) { echo $off_page_bg; } ?>">
	
	<?php 
	// different logo image for different background colors
	if ( $off_page_bg === 'dark-bg' ) {
		$img_color = $off_page_bg;
	} else {
		$img_color = 'light-bg';
	}
	?>

	<nav id="menu" role="navigation" class="<?php echo $off_menu_bg; ?>">
		<div class="mini-nav text-tiny-caps clear">
			<ul>
				<li class="current-menu-item"><a></a></li>
				<li class="mini-nav-button"><a>Menu</a></li>
				<li class="close-menu float-right"><a>&#10005;</a></li>
			</ul>
		</div>
		<hr class="no-margin mini-nav clear">
		<div class="wrapper clear nav-container">
			<ul id="menu-main-menu" class="menu">
				<?php 

				/* 
				* a few conditional / custom menu items
				* -- Home (so it can always be first)
				* -- Artists (based on home page module display settings)
				* -- Exhibitors (based on home page module display settings)
				*/ 

				// get home page object
				$home_page = get_page_by_title('Home');
				$link_class = '';
				// check if we're on the home / front page
				if ( is_front_page() || is_home() ) : 
					$link_class = 'current-menu-item current_page_item'; 
				endif;
				// home page menu item
				echo '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-'.$home_page->ID.' '.$link_class.'"><a href="'.esc_url( home_url( '/' ) ).'">Home</a></li> ';
				
				// check home page module display settings
				// if Artists & Exhibitors module is set to display,
				// then show Artists & Exhibitors links
				if ( get_field('module_exhibitors', $home_page->ID ) ):
					// reset
					$link_class = '';
					// get the queried post type
					// is_post_type() throws an error
					$queried_post_type = get_query_var('post_type');

					if ( $queried_post_type == 'off_artist' ) : $link_class = 'current-menu-item'; endif;
					// artists menu item
					echo '<li class="menu-item menu-item-type-post_type_archive menu-item-object-off_artist '.$link_class.'"><a href="'.esc_url( home_url( '/artists' ) ).'">Artists</a></li> ';
					
					// reset
					$link_class = '';

					if ( $queried_post_type == 'off_exhibitor' ) : $link_class = 'current-menu-item'; endif;
					// exhibitors menu item
					echo '<li class="menu-item menu-item-type-post_type_archive menu-item-object-off_exhibitor '.$link_class.'"><a href="'.esc_url( home_url( '/exhibitors' ) ).'">Exhibitors</a></li> ';

				endif;
				
				// fetch the rest of the menu items
				$args = array(
						'theme_location' => 'main-menu',
						'container' => '',
						'depth' => 1,
						'items_wrap' => '%3$s'
					);
				wp_nav_menu( $args ); 
				
				?>
				<li></li>
			</ul>
		</div>
		<hr class="no-margin">
	</nav>


	<div id="masthead" class="wrapper">
		<div class="clear full-width" style="overflow:hidden;">
			<div id="logo" class="column ninth-4 large-text no-padding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo $off_graphics_prefix.$img_color.'.png'; ?>">
					<h1>
						1:54<br>
						Contemporary<br>
						African&nbsp;Art&nbsp;Fair
					</h1>
				</a>
			</div>
			<div id="header-locations" class="column ninth-5 large-text no-padding">
				<div class="location column quarter-2">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="multiple-choice <?php if(!is_page('about-154-london')){echo 'active';} ?>">
						<span class="break">New York</span>
						<span class="break">May <span class="keep">15–17</span></span>
						<span class="break">2015</span>
					</a>
				</div>
				<div class="location column quarter-2">
					<a href="http://1-54.com/new-york/about-154-london/" class="multiple-choice <?php if(is_page('about-154-london')){echo 'active';} ?>">
						<span class="break">London</span>
						<span class="break">October <span class="keep">15–18</span></span>
						<span class="break">2015</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<hr class="no-margin">
</header>
