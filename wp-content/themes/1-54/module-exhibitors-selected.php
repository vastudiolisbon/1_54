<?php
/**
 * Selected Artists / Exhibitors
 *
 * Display a random selection of 3 or 4 artists / exhibitors
 * 
 * @param post_type
 * @param edition
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

$module_post_type = '';
$module_edition = '';

$module_bg = '';
$header_text_size = 'medium-text';
$module_text_size = 'large-text';
$posts_per_page = 8;
$column_width = 'quarter';

$query_extension = '';

// artists or exhibitors?
if ( get_query_var('post_type') ) {
	$module_post_type = get_query_var('post_type');
} else {
// default to artists (it's probably the home page)
	$module_post_type = 'off_artist';
}

if ( is_singular(array('off_artist','off_exhibitor')) ) { 
	$module_bg = 'dark-bg'; 
}
if ( is_post_type_archive(array('off_artist','off_exhibitor')) ){
	$header_text_size = 'text-tiny-caps';
	$module_bg = 'dark-bg'; 
}

// if we're searching for a particular edition year ...
if ( $off_archive_edition ) {
	// only show artists / exhibitors from that year
	$module_edition  = $off_archive_edition;
	$module_bg       = 'light-bg edition-'.$module_edition; 
	$query_extension = '?edition='.$module_edition;
	
} else {
// only show the current artists / exhibitors
	$module_edition = $off_current_edition;
	$module_bg = 'dark-bg'; 
}

if ( is_front_page() ) {
	$module_bg = $off_page_bg;
}

$args = array(
	'posts_per_page' => $posts_per_page,
	'post_type' => $module_post_type,
	// only show artists / exhibitors who have thumbnails
	'meta_key'    => '_thumbnail_id',
	'edition' => $module_edition,
	'orderby' => 'rand'
); 

$module_query = new WP_Query( $args );

// only display the module if there's something to write home about
if ( $module_query->have_posts() ) : ?>

<section class="module selected-exhibitors <?php //echo $module_bg; ?><?php if(is_post_type_archive(array('off_artist','off_exhibitor'))){echo " archive";} ?>">
	<div class="wrapper clear">
		
		<header class="section-header align-center">
			<h1 class="entry-title medium-text">
				Selected <?php echo get_post_type_object($module_post_type)->labels->name; ?>
			</h1> 
		</header>
		
		<section class="section-content clear">

			<section class="module slideshow">
				<div id="slideshow-container" class="center hidden">
					<div id="slideshow-tray">

		<?php while ( $module_query->have_posts() ) : $module_query->the_post(); ?>

			<div class="slide exhibitor">

				<a href="<?php echo get_the_permalink().$query_extension; ?>" rel="bookmark">
					<?php 
					if ( has_post_thumbnail() ) {

						$size = 'off_thumb_exhibitor_2x';
						echo '<figure class="thumbnail">';
							the_post_thumbnail($size);
						echo '</figure>';
					}
					?>

					<h2 class="small-text align-center"><?php the_title(); ?></h2>
				</a>

			</div>

		<?php endwhile; ?>

					</div>
				</div>
			</section>
		</section>

		<?php if ( is_front_page() ) : ?>
			<div class="clear relative view-all-artists">
				<a href="<?php echo get_the_permalink($forum_page_id); ?>" class="button align-center">View All Artists</a>
			</div>
		<?php endif; ?>
		
	</div>
</section>

<?php endif; wp_reset_query(); ?>