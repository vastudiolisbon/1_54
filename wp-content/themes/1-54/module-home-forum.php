<?php
/**
 * Forum Module
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>

<?php if (have_rows('module_forum_events')) : ?>

<?php 

// used below to force modules into rows
$counter = 1;

if ( get_field('module_forum_page') ) {
	$forum_page_id = get_field('module_forum_page');
}

$forum_bg = get_option('off_graphics_forum_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_forum-bg.png') 

?>

<section class="module forum relative">
	<!-- background pattern -->
	<div class="forum-pattern absolute" style="background:url(<?php echo $forum_bg; ?>); background-position: center center;"></div>
	
	<div class="wrapper clear">

		<header class="section-header relative quarter-2">
			<h1 class="entry-title large-text">
				<?php echo get_the_title($forum_page_id); ?>
			</h1>
		</header>

		<section class="relative section-content clear masonry js-masonry" data-masonry-options='{ "columnWidth":".forum-block", "itemSelector":".forum-block", "transitionDuration":0 }'>

		
		<?php while (have_rows('module_forum_events')) : the_row(); ?>

			<?php 

			$event_date  = get_sub_field('forum_link_date');
			$event_title = get_sub_field('forum_event_title');
			$event_link  = get_sub_field('forum_event_anchor');

			?>

			<div class="forum-block column third">
				<a href="<?php echo $event_link; ?>">
					<header class="entry-header text-tiny-caps">
						<span class="date"><?php echo $event_date; ?></span>
					</header>
					<h2 class="x-large-text">
						<b><?php echo $event_title; ?></b>
					</h2>
				</a>
			</div>

		<?php 

		if ( $counter == 3 ) {
			echo '<div class="clear"></div>';
		}

		$counter++;

		?>

		<?php endwhile; ?>

		</section>
		
		<div class="clear relative">
			<a href="<?php echo get_the_permalink($forum_page_id); ?>" class="button align-center">View All Forum Events</a>
		</div>

	</div>
</section>

<?php endif; ?>