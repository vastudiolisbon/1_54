<?php
/**
 * Features Carousel
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<section class="module slideshow no-header center relative">
<div id="slideshow-container" class="clear">
<!-- <div id="slideshow-tray" style="width:100%;display:block;overflow:hidden;"> -->
<ul class="bxslider" style="width:100%;display:block;overflow:hidden;">
<?php

while ( have_rows('module_features', $post->ID) ) : the_row();

$slide_id          = "";
$slide_object      = "";
$slide_object_type = "";
$slide_text        = "";
$slide_header      = "";
$slide_title       = "";
$slide_img         = "";
$slide_img_id      = "";

$default_category = get_option('default_category');
$categories = "";

// get the linked post, page, etc.
if ( get_sub_field('module_feature_slide') ) {
	// slide ID
	$slide_id = get_sub_field('module_feature_slide');
	// full post object
	$slide_object = get_post($slide_id);
	// post type
	$slide_object_type = $slide_object->post_type;

}

// get the text size: S, L, XL
if ( get_sub_field('module_feature_slide_text_size') ) {
	// text size
	$slide_text = get_sub_field('module_feature_slide_text_size');
} else {
// set a default
	$slide_text = "large-text";
}

// SMALL: excerpt-based
if ( $slide_text == "small-text" ) {
	// post title
	$slide_header = get_the_title($slide_id);
	// post excerpt
	$slide_title = $slide_object->post_excerpt;
	// generate an excerpt if there isn't one
	if ( empty($slide_title) ) {
		// get the content
		$slide_title = $slide_object->post_content;
		// trim it
		$slide_title = strip_tags(strip_shortcodes(limit_text($slide_title, 50)));
	}
	
	$slide_text = "";

} else {
// LARGE & X-LARGE

	// LARGE: headline-ish
	if ( $slide_text == "large-text" ) {
		// slide header = post type
		if ($slide_object_type == "post") {
			// get the post categories
			$categories = wp_get_post_categories($slide_id);
			$prefix = '';

			foreach ($categories as $cat) {
				// category object
				$category = get_category($cat);
				// skip the default category
				if ( $category->term_id != $default_category ) {

					$slide_header .= $prefix . $category->name;
					// $prefix = ', ';
					// split after we get one matching term
					break;
				}
			}

			if ( strlen($slide_header) < 1 ) {
				$slide_header = "News";
			}

		}
		if ($slide_object_type == "page") {
			// 1:54
			$slide_header = "1:54";
		}
		// should have big text
		if (($slide_object_type == "off_artist") || ($slide_object_type == "off_exhibitor")) {
			// artists / exhibitors
			// $slide_object_type = get_post_type_object($slide_object_type);
			// $slide_header = 'Featured '.$slide_object_type->labels->singular_name;
			$slide_text = "xx-large-text";
		}
	}

	// X-LARGE
	if ( $slide_text == "xx-large-text" ) {
		// check if it's an artist or exhibitor
		if (($slide_object_type == "off_artist") || ($slide_object_type == "off_exhibitor")) {
			// get post type object labels
			$slide_object_type = get_post_type_object($slide_object_type);
			$slide_header = 'Featured '.$slide_object_type->labels->singular_name;
		} else {
			// play it safe
			$slide_header = "1:54";	
		}
	}
	// title = title
	$slide_title = get_the_title($slide_id);
}

// get the image data
$slide_img_id = get_post_thumbnail_id( $slide_object->ID );
$slide_img_data = wp_get_attachment_image_src( $slide_img_id, 'off_carousel' );

?>
<li class="">
	<header class="entry-header text-tiny-caps">
		<?php echo $slide_header; ?>
	</header> 
	<a href="<?php	echo get_the_permalink($slide_object->ID); ?>">
		<div class="thumbnail">
			<img src="<?php echo $slide_img_data[0]; ?>" />
		</div>
		<p class="<?php echo $slide_text; ?>">
			<?php echo $slide_title; ?>
		</p>
	</a>
</li>

<?php endwhile; ?>
</ul>
<!-- </div>	 -->
</div>
</section>