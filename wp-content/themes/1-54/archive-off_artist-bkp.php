<?php
/**
 * Artist archive page
 *
 * Same markup as exhibitor archive page
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="module-container no-padding">

<?php include(locate_template('module-exhibitors-archive.php' )); ?>

</section>

<?php include(locate_template('footer.php' )); ?>