<?php
/**
 * Info Module
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php

$info_ad_class = "";
$info_post_id  = get_field('module_big_info', $post->ID );

if ( get_field('module_big_info_show_ads', $post->ID) ) { 
	$ad_class = "sponsors"; 
}

// post excerpt
$info_post_excerpt = get_post($info_post_id)->post_excerpt;
// generate an excerpt if there isn't one
if ( empty($info_post_excerpt) ) {
	// get the content
	$info_post_excerpt = get_post($info_post_id)->post_content;
	// trim it
	$info_post_excerpt = strip_tags(strip_shortcodes(limit_text($info_post_excerpt, 50)));
}

?>
<section class="module big-info large-text light-bg go-behind-button">
<div class="wrapper clear">

	<header class="section-header quarter-2">
		<h1 class="entry-title large-text">
			<?php echo get_the_title($info_post_id); ?>
		</h1>
	</header>

	<section class="section-content clear <?php echo $ad_class; ?>">

		<div class="info-text ninth-7">
			<p class="xx-large-text">
				<?php echo $info_post_excerpt; ?>
			</p>
			<a href="<?php echo get_the_permalink($info_post_id); ?>" class="button align-center">Read More</a>
		</div>

		<?php if ( get_field('module_big_info_show_ads', $post->ID) ) : ?>
			<!-- ads -->
			<?php include(locate_template('module-adverts.php' )); ?>
		<?php endif; ?>

	</section>

</div>
</section>