<?php
/**
 * Home Page
 *
 * To be used as the home page for all 1:54 sites. Requires that a page titled "Home" be created on the site.
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="module-container no-padding">

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<!-- <div class="wrapper clear">
		<?php edit_post_link(); ?>
	</div> -->

	<!-- features -->
	<?php if ( have_rows('module_features', $post->ID ) ): ?>
		<?php include(locate_template('module-home-features.php' )); ?>
	<?php endif; ?>

	<!-- info module -->
	<?php if ( get_field('module_big_info', $post->ID ) ): ?>
		<?php include(locate_template('module-home-info.php' )); ?>
	<?php endif; ?>	

	<!-- artists & exhibitors -->
	<?php if ( get_field('module_exhibitors', $post->ID ) ): ?>
		<?php include(locate_template('module-exhibitors-selected.php' )); ?>
		<?php include(locate_template('module-exhibitors-list.php' )); ?>
	<?php endif; ?>

	<!-- forum -->
	<?php if ( get_field('module_forum', $post->ID ) ): ?>
		<?php include(locate_template('module-home-forum.php' )); ?>
	<?php endif; ?>

	<?php endwhile; endif; ?>

</section>

<?php include(locate_template('footer.php' )); ?>
