<?php
	/*
	Template Name: Homepage
	*/
?>

<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="news-archive">
	<div class="wrapper">
		<section class="clear">
			<?php
				echo "<div class='news-list'>";

				$args = array(
					'post_type' => 'post',
					'fields' => 'ids',
					'status' => 'publish',
					'posts_per_page' => 6
				);

				$total_news = new WP_Query( $args );

				while ( $total_news->have_posts() ) : $total_news->the_post();
		          	$imgurl = get_the_post_thumbnail_url(get_the_id(), 'large');

		          	echo "<div class='news-element'>";
					echo "<a href='" . get_permalink() . "' class='thumbnail' style='background-image: url(" . $imgurl . ");'></a>";
					echo "<a href='" . get_permalink() . "' class='title'>" . get_the_title() . "</a>";

					echo "<div class='excerpt serif'>";
					the_excerpt();
					echo "</div>";

					echo "</div>";

		        endwhile;
		        wp_reset_postdata();

		        echo "</div>";
			?>

			<div class="read-more sans">
				<a href="<?php echo get_permalink(5695); ?>">Read More</a>
			</div>
		</section>
	</div>
</section>


<section role="main" class="center-wrapper">
	<div class="wrapper">
		<div class="clear">
			<section class="entry-content ninth-5 serif relative">
				<h2 class="align-center">ABOUT 1-54</h2>
				
				<div>
				<?php
					$about_page = get_post(5693); 
					echo $about_page->post_excerpt;
				?>
				</div>

				<div class="read-more sans">
					<a href="<?php echo get_permalink(5693); ?>">Read More</a>
				</div>
			</section>
		</div>	
	</div>
</section>

<?php include(locate_template('footer.php' )); ?>

<div class="intro-banner">
		<div class="intro-banner-inner">
			<p class="banner-descritption">
			54<br>
			<span>countries</span><br>
			1<br>
			<span>continent</span><br>
			1<br>
			<span>fair</span>
			<p>
			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 345.2 502.5" style="enable-background:new 0 0 345.2 502.5;" xml:space="preserve">
				<path d="M307.2,402.4V233H272L172,364.3c-13.4-28.1-40.5-44.9-76.6-44.9c-22.5,0-44.2,9.7-56.2,26v-81.8h126.4V233h-63.2V0H67.5
					v30.6H5v30.6h62.5V233H5v148.1h33.8c10.1-19.8,26.4-31.4,50.4-31.4c31.8,0,53.5,14.7,53.5,42.3v34.5c0,28.3-22.9,45-54.3,45
					c-27.9,0-46.3-12.5-52.3-38.4c0-0.1,0-0.2-0.1-0.3l-36,0.3c7.3,44.3,38.8,69.4,89.6,69.4c47.9,0,80.8-26.7,89.6-69.4h93.2v63.6h34.9
					v-63.6h38v-30.6H307.2z M272.3,402.4h-89.6l89.6-117.2V402.4z"/>
			</svg>
			<p class="click-msg">Click to continue</p>
		</div>
	</div>