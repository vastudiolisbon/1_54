/*

Loaded to all pages

*/

jQuery(document).ready(function($){ 

	if($(".intro-banner").length) {
		setTimeout(function(){
			$(".intro-banner").fadeOut(200);
			
		}, 5000);
		$(".intro-banner").click(function(){
			$(this).fadeOut(200);
		});
	}


	$('.newsletter-link a').click(function() {
		$('.newsletter-dialog').addClass('open');
		//$('.newsletter-dialog').find('input:first-of-type').focus();
		return false;
	});

	$('.newsletter-dialog .dismiss').click(function() {
		$('.newsletter-dialog').removeClass('open');
		$('.newsletter-dialog .message').removeClass('error success').empty();

		return false;
	});

	$('.newsletter-dialog').click(function(e) {
		if(e.target == this){
	       $('.newsletter-dialog').removeClass('open');
	       $('.newsletter-dialog .message').removeClass('error success').empty();
	   }
	});

	$('#subscribe_newsletter').on('submit', function() {
		var first_name = $("input[name='first_name']").val();
		var last_name = $("input[name='last_name']").val();
		var email = $("input[name='email']").val();

		$.ajax({
			type: 'POST',
			url: '/wp-admin/admin-ajax.php',  
			data: { action: 'CCAjax', first_name: first_name, last_name: last_name, email: email},  
			timeout: 10000,
			success: function(result) {
				result = JSON.parse(result);

				if(result.success == false) {
					$('.newsletter-dialog .message').removeClass('success').addClass('error');
					$('.newsletter-dialog .message').empty().append("<p>There was an error with your subscription. Try again.</p>");	
				}
				else {
					$('.newsletter-dialog .message').removeClass('error').addClass('success');
					$('.newsletter-dialog .message').empty().append("<p>Thanks for subscribing!</p>");

					setTimeout(function() {
						$('.newsletter-dialog').removeClass('open');
						$('.newsletter-dialog .message').removeClass('error success').empty();
					}, 2000);
				}
	    	},
	    	error: function(){
	      		//alert('não foi possível');
	       }
	    });

		return false;
	});



	////


	if($('#content').hasClass('activate-archive-link')) {
		$('.secondary-nav a[href*="archive"]').each(function() {
		    $(this).parent('li').addClass('current-menu-item');
		});
	}

	if($('#content').hasClass('activate-artists-link')) {
		$('.secondary-nav a[href*="artists"]').each(function() {
		    $(this).parent('li').addClass('current-menu-item');
		});
	}

	if($('#content').hasClass('activate-exhibitors-link')) {
		$('.secondary-nav a[href*="exhibitors"]').each(function() {
		    $(this).parent('li').addClass('current-menu-item');
		});
	}

	if($('#content').hasClass('activate-news-link')) {
		$('.main-nav a[href*="news"]').each(function() {
		    $(this).parent('li').addClass('current-menu-item');
		});
	}

	if($(window).width() > 1000) {
		$(".secondary-nav").sticky({topSpacing:90});
		$(".tickets .button").sticky({topSpacing: 17});
		$('body, html').removeClass('overflow-hidden');
	}

	$(window).resize(function() {
		if($(window).width() > 1000) {
			$(".secondary-nav").sticky({topSpacing: 90});
			$(".tickets .button").sticky({topSpacing: 17});
			$('body, html').removeClass('overflow-hidden');
		}
		else {
			$(".secondary-nav").unstick();
			$(".tickets .button").unstick();
			$('#footer').removeClass('fixed');
		}
	});

	var footerAnimating = false;

	$('.hamburger').click(function() {
		if($(this).hasClass('active')) {
			$(this).removeClass('active')
			$('#header').removeClass('expanded');

			$('body, html, body > main').removeClass('overflow-hidden');
			//$('body > main').show();
		}

		else {
			$(this).addClass('active')
			$('#header').addClass('expanded');

			$('body, html, body > main').addClass('overflow-hidden');
			//$('body > main').hide();
		}

		return false;
	});

	$('.cities-nav .wrapper > ul > li > a').click(function() {
		if($(window).width() < 1000) {
			// $('.cities-nav .wrapper > ul > li > a.expanded').removeClass('expanded');

			$(this).toggleClass('expanded');

			return false;
		}
	});

	(function () {
	    var _overlay = document.getElementById('scrollable');
	    var _clientY = null; // remember Y position on touch start

	    _overlay.addEventListener('touchstart', function (event) {
	        if (event.targetTouches.length === 1) {
	            // detect single touch
	            _clientY = event.targetTouches[0].clientY;
	        }
	    }, false);

	    _overlay.addEventListener('touchmove', function (event) {
	        if (event.targetTouches.length === 1) {
	            // detect single touch
	            disableRubberBand(event);
	        }
	    }, false);

	    function disableRubberBand(event) {
	        var clientY = event.targetTouches[0].clientY - _clientY;

	        if (_overlay.scrollTop === 0 && clientY > 0) {
	            // element is at the top of its scroll
	            event.preventDefault();
	        }

	        if (isOverlayTotallyScrolled() && clientY < 0) {
	            //element is at the top of its scroll
	            event.preventDefault();
	        }
	    }

	    function isOverlayTotallyScrolled() {
	        // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
	        return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
	    }
	}())

	//REMOVE HOVER STATES ON TOUCH DEVICES
	var touch = 'ontouchstart' in document.documentElement || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);

	if (touch) { // remove all :hover stylesheets
		try { // prevent exception on browsers not supporting DOM styleSheets properly
			for (var si in document.styleSheets) {
				var styleSheet = document.styleSheets[si];
				if (!styleSheet.rules) continue;

				for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
					if (!styleSheet.rules[ri].selectorText) continue;

					if (styleSheet.rules[ri].selectorText.match(':hover')) {
						styleSheet.deleteRule(ri);
					}
				}
			}
		} catch (ex) {}
	}

	$('.search-wrapper .search-btn').click(function() {
		$(this).parent('.search-wrapper').toggleClass('expanded');
		$(this).parent(".search-wrapper").find("form input[type='text']").focus();
		return false;
	});

	// page scroll position
	var _scrollPos = $(window).scrollTop()
		// window height
		, _winHeight = $(window).height()
		// window width
		, _winWidth = $(window).width()
		// document height
		, _docHeight = $(document).height()
		// fixed nav height (effects scroll bottom position)
		, _headerHeight = $('#header nav').height()
		// footer height
		, _footerHeight = $('#footer').height()
		// button element (only one per page)
		, _actionButton = $('#action-button')
		// button height
		, _actionButtonHeight = $('#action-button').height()
		// set via stylesheet, button is fixed 40px from the bottom of the window
		, _actionButtonBottom = 40
		// position of scroll at window's bottom edge
		, _scrollBottom = _scrollPos + _headerHeight + _winHeight
		;

	$('#newsletter input').focus(function(){
		$(this).parents('#newsletter').find('#signup-blind').addClass('unblind');
	});
	
	$('#newsletter input').blur(function(){
		$(this).parents('#newsletter').find('#signup-blind').removeClass('unblind');
	});

	$(window).bind({

		load : function(){
			// document height
			_docHeight = $(document).height()
			watchButton();
			updateWinDimensions();
			animateNav();
			miniNav();
		},
		scroll : function(){
			// temporarily, until extra space issue on pages is resolved. ugh
			// sets left scroll to 0px
			// $(this).scrollLeft(0); 
			setTimeout(function(){
				watchButton();
				animateNav();
			}, 30 );
		},
		resize : function(){
			setTimeout(function(){
				watchButton();
				updateWinDimensions();
			}, 30 );
		}

	});

	function miniNav() {
		var linkText = $('#menu-main-menu .current-menu-item').find('a').text()
			, miniNavCurrentItem = $('.mini-nav .current-menu-item a');
		
		miniNavCurrentItem.text(linkText);

		$('.mini-nav').click(function(){
			$('body').toggleClass('mini-nav-open');
			$('#menu').toggleClass('open');
		});
	}


	function updateWinDimensions() {
		// update dimensions
		_winHeight = $(window).height()
		, _winWidth = $(window).width()
		, _docHeight = $(document).height()
		;
		// store them on the doc `body` so other scripts can use this info
		$('body').attr('data-win-width', _winWidth ).attr('data-win-height', _winHeight ).attr('data-doc-height', _docHeight);

	}

	function animateNav() {

		_docHeight = $(document).height()
		, _scrollPos = $(window).scrollTop()
		;

		if ( _docHeight > 500 ) {

			if ( _scrollPos >= 200 ) {

				if( !$('body').hasClass('scrolled') ) {
					$('#menu').css('top','-150px').animate({
						top : 0
					}, 100 );
					$('body').addClass('scrolled');
				}

			} else {
				$('body').removeClass('scrolled');
			}
		} else {
			$('body').removeClass('scrolled');
		}
	}

	// offset button from backgrounds of the same color
	function	watchButton() {

		// if _actionButton exists
		if ( _actionButton.length ) {

			// update scroll positions
			_scrollPos = $(window).scrollTop()
			, _winHeight = $(window).height()
			, _scrollBottom = _scrollPos + _headerHeight + _winHeight
			;

			$('.go-behind-button').each(function(){

				// top position of the background module
				bgModuleTop = $(this).offset().top
				// bottom position of the background module
				, bgModuleBottom = bgModuleTop + $(this).height()
				;

				// if the top of the background module reaches the bottom of the button ...
				if ( (bgModuleTop + _actionButtonBottom*2) <= _scrollBottom && 
					// ... and the bottom of the background module reaches above the middle of the botton
					(bgModuleBottom + _actionButtonBottom + _actionButtonHeight*1.5 ) >= _scrollBottom ) {

					// offset the button
					if ( !_actionButton.hasClass('shadowed') ) {
						_actionButton.addClass('shadowed');
					}
				} else {
				// flat button is fine
					_actionButton.removeClass('shadowed');
				}
			});

		}
	} // watchButton()	

	$('.artists-exhibitors .multiple-choice').each(function(){

		$(this).bind({

			click : function(e) {

				e.preventDefault();

				var choice = $(this).attr('data-choice')

				$('.artists-exhibitors .multiple-choice').removeClass('active').parents().find('.choice').removeClass('active');

				$(this).addClass('active').parents().find('.choice.'+choice).addClass('active');

			}

		});

	})


	$('.artists-exhibitors .list a').each(function(){

		$(this).bind({

			mousemove : function(event) {
				// only for non-mobile devices
				if ( $('body').attr('data-mobile') == "false" && $('body').attr('data-win-width') > 600 ) {
						// text or image
					var hoverType = $(this).attr('data-hover-type')
						// content to display
						, hoverContent = $(this).attr('data-hover-content')
						// containing element
						, hoverContainer = $(this).parent('li').find('.hover.preview')
						// max width of container, set via CSS
						, hoverMaxWidth = 250 
						// x, y offset from mouse position
						, hoverOffsetX = 10
						, hoverOffsetY = 20
						// mouse position
						, mouseX = event.clientX
						, mouseY = event.clientY
						// hover element position
						, hoverX = mouseX + hoverOffsetX
						, hoverY = mouseY + hoverOffsetY
						// window dimensions
						, _winWidth = $('body').attr('data-win-width')
						, _winHeight = $('body').attr('data-win-height')
						;

					if ( hoverContent.length ) {
						
						// if hoverContainer doesn't exist yet
						if ( hoverContainer.length < 1 ) {
							// build it
							hoverContainer = $('<div class="'+hoverType+' hover preview fixed hidden">'+hoverContent+'</div>');
							// add it to the dom
							$(this).parent('li').append( hoverContainer );
						}

						// if hover preview might be displayed offscreen
						if ( (hoverX + hoverMaxWidth) >= _winWidth - hoverOffsetX ) {
							// put it on the left side of the mouse
							hoverX = mouseX - hoverOffsetX - hoverMaxWidth;
							// re. height: user can scroll up / down to correct
						}
						// position and display the hover preview
						hoverContainer.css({
							'left' : hoverX + 'px',
							'top' : hoverY + 'px'
						}).fadeIn(100);

					}
 				}
			},
			mouseleave : function() {
				// hide the preview
				$(this).parent('li').find('.hover.preview').delay(30).fadeOut(100);
			}

		});

	});

});