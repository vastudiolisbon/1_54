jQuery(document).ready(function($){

	var _vimeoPrefs = "?color=ffffff&title=0&byline=0&portrait=0"
		// get all the videos
		, _allVideos = $("iframe[src*='youtube.com'], iframe[src*='vimeo.com']")
		// el that is fluid width
		, _fluidEl = $(".entry-content")
		;

	_allVideos.each(function(){
		// add vimeo preferences where applicable
		appendPreferences( $(this), _vimeoPrefs );

	});

	// Figure out and save aspect ratio for each video
	_allVideos.each(function() {

		$(this)
			.data('aspectRatio', this.height / this.width)
			// and remove the hard coded width/height
			.removeAttr('height')
			.removeAttr('width');

	});

	$(window).resize(function() {

		var newWidth = _fluidEl.width();
		// Resize all videos according to their own aspect ratio
		_allVideos.each(function() {

			var $el = $(this);

			$el
				.width(newWidth)
				.height(newWidth * $el.data('aspectRatio'));
		});
		
	// Kick off one resize to fix all videos on page load
	}).resize();

	// append desired vimeo settings to the iframe source URL
	function appendPreferences(video, videoSettings) {
		// get the url
		videoURL = video.attr('src');
		// make sure it's vimeo, and that no settings have been declared
		if ( videoURL.indexOf("vimeo") >= 0 && videoURL.indexOf("?") < 0 ) {
			// append desired settings to the video URL
			videoURL += videoSettings;
			// apply the new settings
			video.attr('src', videoURL);
		}
	}

});

