jQuery(document).ready(function($){

	var headers = $('.entry-content h1, .entry-content h2, .entry-content h3')
		, windowLocation = window.location.href.replace(window.location.hash, '')
		;

	if ( $('body').hasClass('logged-in admin-bar') ) {

		headers.each( function() {

			var anchorId = $(this).find('.section-anchor').attr('id');

			$(this).append('<div class="anchor-details" style="padding:0 0 40px;"><div class="sans light-bg shadowed"><a href="' + windowLocation + '#' + anchorId + '">' + windowLocation + '#' + anchorId + '</a></div></div>');

		});

	}

});