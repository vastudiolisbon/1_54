jQuery(document).ready(function($){

	var _directory = $('body').data('directory')
		, _winWidth = $('body').attr('data-win-width')
		, _winHeight = $('body').attr('data-win-height')
		, _slickStyles = '<link rel="stylesheet" href="'+_directory+'/assets/js/vendor/slick/slick.css">'
		;	

	$(_slickStyles).insertBefore('head link[rel="stylesheet"]:first');

	$('#slideshow-tray:not(.hero)').slick({
		dots: false
		, arrows: true
		, appendArrows: $('#slideshow-tray')
		, prevArrow: '<div class="nav prev absolute align-left"><div class="arrow absolute"></div></div>'
		, nextArrow: '<div class="nav next absolute align-right"><div class="arrow absolute"></div></div>'
		, slidesToShow: 3
		, slidesToScroll: 1
		, adaptiveHeight: true
		, infinite: false
		, responsive: [
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 2
				}		
			}, {
				breakpoint: 600,
				settings: {
					slidesToShow: 1
					, adaptiveHeight: true
				}
			}
		]

	});

	$('#slideshow-tray.hero').slick({
		dots: false
		, arrows: true
		, appendArrows: $('#slideshow-tray')
		, prevArrow: '<div class="nav prev absolute align-left"><div class="arrow absolute"></div></div>'
		, nextArrow: '<div class="nav next absolute align-right"><div class="arrow absolute"></div></div>'
		, slidesToShow: 1
		, slidesToScroll: 1
	});

	$('#slideshow-container').fadeIn(500);

});