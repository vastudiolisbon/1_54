jQuery(document).ready(function($){

	var vimeoPrefs = "?color=ffffff&title=0&byline=0&portrait=0"
		, videos = $("iframe[src*='vimeo.com'], iframe[src*='youtube.com']")
		;

	// if iframes exist, go!
	if ( $("iframe").length > 0 ) {

		// if video embeds exist, and they are not wrapped
		if ( videos.length >= 1 && $(".video-container").length < 1 ) {
			// wrap them
			wrapVideos(videos);
		}

		// add vimeo preferences, if not already applied
		$(".video-container").each(function(){
			appendPreferences( $(this).find('iframe'), vimeoPrefs );
		})
		// apply responsive script to video, but not audio
		.fitVids({
			ignore: '#issue-audio'
		});

	}


	// check to see if embedded iframes contain the videos we're looking for
	function wrapVideos(videos){
		// find all the video embeds
		videos.each(function(){
			// wrap each one in .video-container
			$(this).wrap('<div class="video-container"></div>');

		});
	}

	// append desired vimeo settings to the iframe source URL
	function appendPreferences(video, videoSettings) {
		// get the url
		videoURL = video.attr('src');
		// make sure it's vimeo, and that no settings have been declared
		if ( videoURL.indexOf("vimeo") >= 0 && videoURL.indexOf("?") < 0 ) {
			// append desired settings to the video URL
			videoURL += videoSettings;
			// apply the new settings
			video.attr('src', videoURL);
		}
	}

});