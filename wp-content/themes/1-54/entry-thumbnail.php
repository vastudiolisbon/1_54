<?php
/**
 * Entry Thumbnail
 *
 * Used on single posts and pages. Relies on `the_post_thumbnail_caption()`, in functions.php
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?> 
<?php if ( has_post_thumbnail() ) : ?>
<?php 

	$size = "";
	if (get_post_type() == 'post' && is_page('news')) {
		$size = 'off_thumb_2x';
	} 

?>
<figure class="thumbnail">

	<?php	the_post_thumbnail($size); ?>

	<?php 
	if ( is_single() && !is_page_template('page-news.php') ) { 
		// the_post_thumbnail_description(); 
		echo '<figcaption class="wp-caption-text ninth-3">'.get_post( get_post_thumbnail_id() )->post_excerpt.'&nbsp;</figcaption>';
	} 
	?>

</figure>

<?php endif; ?>