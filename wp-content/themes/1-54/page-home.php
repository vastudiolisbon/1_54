<?php
/**
 * Home Page
 *
 * To be used as the home page for all 1:54 sites. Requires that a page titled "Home" be created on the site.
 *
 * @package WordPress
 * @subpackage 1:54
 */

/*
	Template Name: City Homepage
*/
?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="module-container no-padding">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php if(have_rows('slideshow')): ?>
	
	<section class="module slideshow full-width">
		<div id="slideshow-container" class="hidden center">
			<div id="slideshow-tray" class="hero">
	
				<?php
					while( the_repeater_field('slideshow') ):
						$imgurl = wp_get_attachment_image_src(get_sub_field('image'), 'large')[0];

						$width = wp_get_attachment_image_src(get_sub_field('image'), 'large')[1];
						$height = wp_get_attachment_image_src(get_sub_field('image'), 'large')[2];

						if (strpos($imgurl, '.gif') !== false || ($width < 2048 && $height < 2048)) {
						    $imgurl = wp_get_attachment_image_src(get_sub_field('image'), 'original')[0];
						}
				?>

				<div class="slide">
					<img src="<?php echo $imgurl; ?>" />
				</div>

				<?php endwhile; ?>

			</div>
		</div>
	</section>

	<?php
		endif;

		if(get_the_content()) {
	?>

		<div style="background-color: <?php echo get_field('content_bg_color'); ?>;" class="center-wrapper">
			<div class="wrapper clear">
				<section class="entry-content ninth-5 serif relative">

					<?php the_content(); ?>

				</section>
			</div>	
		</div>

	<?php
		}
	?>

	<?php endwhile; endif; ?>
</section>

<?php include(locate_template('footer.php' )); ?>
