<?php
/**
 * Template Name: Edition Archive
 *
 * Not to be confused with the taxonomy-edition.php archive
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="edition-archive">

	<div class="wrapper clear">

		<header class="page-header">
			<h1 class="entry-title medium-text align-center">
				<?php get_template_part( 'entry','single-header' ); ?>
			</h1>
		</header>

		<section class="edition-links clear">

			<?php edit_post_link(); ?>

			<?php
			// get all past editions
			$taxonomy = 'edition';

			$args = array(
				'hide_empty'=> false,
				'orderby'=> 'name',
				'order' => 'DESC'
				);

			$past_editions = get_terms($taxonomy,$args);

			if ( $past_editions ) {
				// display a link to each edition
				foreach ( $past_editions as $past_edition ) {
					// && don't display editions newer than current edition
					if ( ($past_edition->slug !== off_get_current_edition()) && (intval($past_edition->slug) < intval(off_get_current_edition())) ) {
						// get id and meta data
						$past_edition_id = $past_edition->term_taxonomy_id;
						$past_edition_meta = get_option( "taxonomy_term_$past_edition_id" );
						$past_edition_light = $past_edition_meta['color_light'];
						$past_edition_dark = $past_edition_meta['color_dark'];

						// build the link
						$link = get_term_link($past_edition);

						?>

						<div class="column third past-edition">
							<?php
							if ( get_edit_term_link($past_edition->term_id,'edition') ) {
								$edit_link = '<a class="post-edit-link" href="'.get_edit_term_link($past_edition->term_id,'edition').'">EDIT THIS</a>';
								echo $edit_link;
							}
							?>
							<a href="<?php echo $link; ?>" class="edition-link edition-<?php echo $past_edition->slug; ?> align-center">
								<h1 class="edition-year medium-text">
									<?php echo $past_edition->name; ?>
								</h1>
								<h2 class="edition-description medium-text">
									<?php echo str_replace('***', '<br>', $past_edition->description); ?>
								</h2>
							</a>
						</div>

						<?php

					}
				}
			}

			?>

		</section>

	</div>

	<?php if (have_rows('alternate_archive_links')) : ?>

		<section class="alternate-edition-links clear">
			<div class="clear relative wrapper">

				<?php

				while (have_rows('alternate_archive_links')) : the_row();

					echo '<a href="'.get_sub_field('archive_link_url').'" class="button align-center" target="_blank">1:54 '.get_sub_field('archive_link_location').' Archive</a>';

				endwhile;

				?>

			</div>
		</section>

	<?php endif; ?>

</section>

<?php include(locate_template('footer.php' )); ?>
