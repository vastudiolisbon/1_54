<?php
/**
 * Artist & Exhibitors list
 *
 * Used on the home page, Artist & Exhibitor single and archive pages
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

$module_display_first = '';
$module_edition = '';

$thumb_url = '';
$hover_content = '';
$hover_content_type = '';

$query_extension = '';

// display artists or exhibitors first?
if ( get_query_var('post_type') ) {
	$module_display_first = get_query_var('post_type');
} else {
// default to artists (it's probably the home page)
	$module_display_first = 'off_artist';
}

// different rules for the home page
if ( !is_front_page() && !is_tax() ) {
	$module_bg = 'white-bg';
// } elseif ( is_tax() ) {
	// $module_bg = 'light-bg';
} else {
	$module_bg = 'opposite-bg';
}

// if we're searching for a particular edition year ...
if ( $off_archive_edition  ) {
	// only show artists / exhibitors from that year
	$module_edition = $off_archive_edition;
	$module_bg = 'light-bg edition-'.$module_edition;
	$query_extension = '?edition='.$module_edition;
} else {
// only show the current artists / exhibitors
	$module_edition = off_get_current_edition();
}

$args = 'posts_per_page=-1&edition='.$module_edition.'&orderby=title&order=ASC';

$artist_query = new WP_Query( $args . '&post_type=off_artist' );

$exhibitor_query = new WP_Query( $args . '&post_type=off_exhibitor' );

if ( $artist_query->have_posts() || $exhibitor_query->have_posts() ) :

	$current_letter = '';

	$special_count = array();
	while ( $exhibitor_query->have_posts() ) : $exhibitor_query->the_post();
		if(get_field('special_project') != false) {
				array_push($special_count, 1);
		}
		endwhile;

?>
<section class="module artists-exhibitors">

	<div class="wrapper clear">
		<?php
			if(!is_singular('off_exhibitor') || get_field('special_project') == false) {
				$classActive1 = 'active';
				$classActive2 = '';
			}

			else {
				$classActive1 = '';
				$classActive2 = 'active';
			}
		?>

		<header class="section-header clear align-center">
			<a href="#" class="entry-title multiple-choice medium-text <?php echo $classActive1; ?>" data-choice="galleries">1-54 EXHIBITORS</a>
			<?php
				if(count($special_count) >= 1){ ?>
					<a href="#" class="entry-title multiple-choice medium-text <?php echo $classActive2; ?>" data-choice="special-projects">1-54 ONLINE ONLY</a>
				<?php } ?>
		</header>

		<section class="section-content clear small-text">
		<!-- galleries list -->
		<div class="list choice galleries <?php echo $classActive1; ?>">
			<ul class="text-columns column-4">

			<?php

			while ( $exhibitor_query->have_posts() ) : $exhibitor_query->the_post(); ?>

				<?php
				if(get_field('special_project') == false) {

				// show hover content
				if (get_field('exhibitor_meta_info')) {
					$hover_content = substr(get_field('exhibitor_meta_info'),0,40);
				} else {
				// nothing to show
					$hover_content = '';
				}

				?>
				<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="text" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

			<?php }else{

			} ?>
			<?php endwhile; ?>

			</ul>
		</div>

		<?php wp_reset_query(); ?>


		<!-- special projects list -->
		<?php if(count($special_count) >= 1){ ?>
			<div class="list choice special-projects <?php echo $classActive2; ?>">
				<ul class="text-columns column-4">

				<?php $special_project_count = 0; ?>
				<?php while ( $exhibitor_query->have_posts() ) : $exhibitor_query->the_post();
					if(get_field('special_project') == true) {
						$special_project_count++;

						// show hover content
						if (get_field('exhibitor_meta_info')) {
							$hover_content = substr(get_field('exhibitor_meta_info'),0,40);
						} else {
						// nothing to show
							$hover_content = '';
						}

				?>

					<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="text" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

				<?php
						}
					endwhile;
				?>
				</ul>

				<?php
					if($special_project_count == 0) {
						echo "No Special Projects to show";
					}
				?>
			</div>


		<?php } wp_reset_query(); ?>

		</section>

	</div>
</section>

<?php endif; ?>
