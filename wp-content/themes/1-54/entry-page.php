<?php
/**
 * Page Module
 *
 * Used on all single page templates
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<div class="wrapper clear">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<header class="page-header align-center">
		<?php if(get_field('show_title') == true || is_singular('post')) { ?>
		<h1 class="entry-title medium-text">
			<?php get_template_part( 'entry','single-header' ); ?>
		</h1>
		<?php } ?> 
	</header>

	<section class="ninth-6 no-padding center">

		<?php edit_post_link(); ?>

		<?php 
		// if user is logged in, display page details
		if ( is_user_logged_in() ) : ?>
		
		<div class="page-details light-bg">
			<p><span class="medium-text">Page Details</span></p>
			<div class="clear">
				<div class="column quarter"><p><span class="text-tiny-caps">Page ID</span></p></div><div class="column quarter-3"><p><?php echo $post->ID; ?></p></div>
				<div class="column quarter"><p><span class="text-tiny-caps">Permalink</span></p></div><div class="column quarter-3"><p><?php the_permalink(); ?></p></div>
				<div class="column quarter"><p><span class="text-tiny-caps">Excerpt</span></p></div><div class="column quarter-3"><?php the_excerpt(); ?></div>
			</div>
		</div>	

		<?php endif; ?>

	</section>

	<section class="entry-content ninth-6 serif no-padding relative">

	<?php

	// get_template_part( 'entry','thumbnail' );

	the_content();

	?>

	<?php if(have_rows('partners_sponsors')): ?>
	
	<section class="partners-list">
		<?php while( the_repeater_field('partners_sponsors') ): ?>
			<div class="partners-row">
				<p class="entry-title small-text sans"><?php echo get_sub_field('title'); ?></p>

				<?php
					while( the_repeater_field('logos') ):
						$imgurl = wp_get_attachment_image_src(get_sub_field('image'), 'full')[0];
				?>

					<?php if(get_sub_field('link')) { ?><a href="<?php echo get_sub_field('link'); ?>" target="blank"><?php } ?>
						<img src="<?php echo $imgurl; ?>">
					<?php if(get_sub_field('link')) { ?></a><?php } ?>

				<?php endwhile; ?>
			</div>
		<?php endwhile; ?>
	</section>

	<?php endif; ?>

	</section>

<?php endwhile; endif; ?>

</div>