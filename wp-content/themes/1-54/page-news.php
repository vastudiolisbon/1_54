<?php
/**
 * Template Name: News Archive
 * 
 * Description: displays all news entries
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'dark-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main">
	<div class="wrapper">
		<header class="page-header align-center">
			<h1 class="entry-title medium-text">News</h1> 
		</header>

		<section class="clear">
			<?php
				echo "<div class='news-list'>";

				$args = array(
					'post_type' => 'post',
					'fields' => 'ids',
					'status' => 'publish',
					'posts_per_page' => -1
				);

				$total_news = new WP_Query( $args );

				while ( $total_news->have_posts() ) : $total_news->the_post();
		          	$imgurl = get_the_post_thumbnail_url(get_the_id(), 'large');

		          	echo "<div class='news-element'>";
					echo "<a href='" . get_permalink() . "' class='thumbnail' style='background-image: url(" . $imgurl . ");'></a>";
					echo "<a href='" . get_permalink() . "' class='title'>" . get_the_title() . "</a>";

					echo "<div class='excerpt serif'>";
					the_excerpt();
					echo "</div>";

					echo "</div>";

		        endwhile;
		        wp_reset_postdata();

		        echo "</div>";
			?>
		</section>
	</div>
</section>

<?php include(locate_template('footer.php' )); ?>