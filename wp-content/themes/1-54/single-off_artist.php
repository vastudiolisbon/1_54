<?php
/**
 * Single Artist Page
 *
 * @package WordPress
 * @subpackage 1:54
 */

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

$off_page_bg = 'white-bg';

include(locate_template('header.php' ));

// set defaults
$off_page_bg         = 'white-bg';
$module_bg           = '';

$terms           = '';
$edition_exists  = false;

$include_archive_nav = false;

$terms = get_the_terms($post->ID, 'edition');

// if page is tagged with the current edition, or has no edition at all ...
if ( ( get_query_var('edition') === '' || has_term( $off_current_edition, 'edition' ) ) || (array) $terms !== $terms ) {

	// do nothing

} else {
// page is tagged with an archived edition

	// check if we're already browsing a particular edition
	if ( get_query_var('edition') ) {
		// only show content from that edition
		$off_archive_edition  = get_query_var('edition');

		$module_bg       = 'light-bg edition-'.$off_archive_edition;
		// use the archive color for the whole page
		$off_page_bg     = $module_bg;
		// display the archive nav
		$include_archive_nav = true;

	}

	// check query var against the editions associated with this page
	if ( $terms && !is_wp_error($terms) ) {

		foreach ($terms as $term) {
			if ( $off_archive_edition == $term->slug ) {
				$edition_exists = true;
				break;
			}
		}
		// if the queried edition doesn't exist ...
		if ( $edition_exists === false ) {
			// use the most recent edition
			$most_recent_term = $terms[count($terms)-1];

			$off_archive_edition  = $most_recent_term->slug;
			$module_bg       = 'light-bg edition-'.$off_archive_edition;
			// use the archive color for the whole page
			$off_page_bg     = $module_bg;
			// display the archive nav
			$include_archive_nav = true;
		}
	}
}

?>

<?php if ( $include_archive_nav === true ) include(locate_template('nav-above-archive.php' )); ?>


<?php
	$activateLinkClass = '';

	if(is_singular('off_artist')) {
		$activateLinkClass = 'activate-artists-link';
	}else if(is_singular('off_exhibitor')) {
		$activateLinkClass = 'activate-exhibitors-link';
	}
?>


<section id="content" role="main" class="<?php echo $activateLinkClass; ?>">
	<section class="module">
		<?php include(locate_template('entry-page-exhibitors.php' )); ?>
	</section>

	<?php include(locate_template('module-exhibitors-selected.php' )); ?>

	<section class="module artists-exhibitors">
		<div class="wrapper clear">
			<header class="page-header align-center ninth-5">
				<h1 class="entry-title medium-text">
					Artists <?php echo $module_edition; ?>
				</h1>
			</header>

			<section class="section-content clear small-text">
				<div class="list artists">
					<ul class="text-columns column-5">

					<?
						// if we're searching for a particular edition year ...
						if ( $off_archive_edition  ) {
							// only show artists / exhibitors from that year
							$module_edition = $off_archive_edition;
							$module_bg = 'light-bg edition-'.$module_edition;
							$query_extension = '?edition='.$module_edition;
						} else {
						// only show the current artists / exhibitors
							$module_edition = off_get_current_edition();
						}

						$args = 'posts_per_page=-1&edition='.$module_edition.'&orderby=title&order=ASC&post_type=off_artist';
						$current_letter = '';
						$artist_query = new WP_Query($args);

						if($artist_query->have_posts()){ while ($artist_query->have_posts()){ $artist_query->the_post() ?>

						<?php
						$this_letter = strtoupper(substr(get_the_title(),0,1));
						if ( $this_letter != $current_letter ) {
							if ($this_letter != 'A') {
								echo '</ul></li>';
							}
							echo '<li><h2 class="medium-text">'.$this_letter.'</h2><ul>';
							$current_letter = $this_letter;
						}
						if ( has_post_thumbnail() ) {
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src( $thumb_id, 'off_thumb_artist_hover', false );
							$hover_content = '<img src=\''.$thumb_url[0].'\'/>';

						} else {
							$hover_content = '';
						}
						?>

						<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="image" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

					<?php }} ?>

					</ul>

				</div>

				<?php wp_reset_query(); ?>
			</section>
		</div>
	</section>
</section>



<?php include(locate_template('footer.php' )); ?>
