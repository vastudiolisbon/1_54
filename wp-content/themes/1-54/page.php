<?php
/**
 * Default Page Template
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

// set defaults
$off_page_bg     = 'white-bg';
$module_bg       = '';

$terms           = '';
$edition_exists  = false;

$include_archive_nav = false;

$terms = get_the_terms($post->ID, 'edition');

// if page is tagged with the current edition, or has no edition at all ...
if ( has_term( $off_current_edition, 'edition' ) || (array) $terms !== $terms ) {

	// different rules based on title
	$search_in = get_the_title();

	// forum page = dark-bg
	if (strpos(strtolower($search_in),'forum') !== false) {
		$off_page_bg = 'dark-bg';
	} 
	// about page = light-bg
	if (strpos(strtolower($search_in),'about') !== false) {
		$off_page_bg = 'light-bg go-behind-button';
	} 

} else {
// page is tagged with an archived edition

	// check if we're already browsing a particular edition
	if ( get_query_var('edition') ) {
		// only show content from that edition
		$off_archive_edition  = get_query_var('edition');
		
		$module_bg       = 'light-bg edition-'.$off_archive_edition;
		// use the archive color for the whole page
		$off_page_bg     = $module_bg;
		// display the archive nav
		$include_archive_nav = true;

	}

	// check query var against the editions associated with this page
	if ( $terms && !is_wp_error($terms) ) {

		foreach ($terms as $term) {
			if ( $off_archive_edition == $term->slug ) {
				$edition_exists = true;
				break;
			}
		}
		// if the queried edition doesn't exist ... 
		if ( $edition_exists === false ) {
			// use the most recent edition
			$most_recent_term = $terms[count($terms)-1];

			$off_archive_edition  = $most_recent_term->slug;
			$module_bg       = 'light-bg edition-'.$off_archive_edition;
			// use the archive color for the whole page
			$off_page_bg     = $module_bg;
			// display the archive nav
			$include_archive_nav = true;
		}
	}
}

?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main">

	<?php if ( $include_archive_nav === true ) include(locate_template('nav-above-archive.php' )); ?>

	<?php get_template_part( 'entry','page' ); ?>

</section>

<?php include(locate_template('footer.php' )); ?>
