<?php
/**
 * Header Nav
 *
 * Appears on all pages, has two styles contingent upon options page selection: dark home page or light home page.
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<header id="header" class="page-row <?php if ( $off_page_bg === 'dark-bg' ) { echo $off_page_bg; } ?>">
	
	<nav id="menu" role="navigation" class="<?php echo $off_menu_bg; ?>">
		<div class="mini-nav text-tiny-caps clear">
			<ul>
				<li class="current-menu-item"><a></a></li>
				<li class="mini-nav-button"><a>Menu</a></li>
				<li class="close-menu float-right"><a>&#10005;</a></li>
			</ul>
		</div>
		<hr class="no-margin mini-nav clear">
		<div class="wrapper clear nav-container">
			<ul id="menu-main-menu" class="menu">
				<?php 

				/* 
				* a few conditional / custom menu items
				* -- Home (so it can always be first)
				* -- Artists (based on home page module display settings)
				* -- Exhibitors (based on home page module display settings)
				*/ 

				// get home page object
				$home_page = get_page_by_title('Home');
				$link_class = '';
				// check if we're on the home / front page
				if ( is_front_page() || is_home() ) : 
					$link_class = 'current-menu-item current_page_item'; 
				endif;
				// home page menu item
				echo '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-'.$home_page->ID.' '.$link_class.'"><a href="'.esc_url( home_url( '/' ) ).'">Home</a></li> ';
				
				// check home page module display settings
				// if Artists & Exhibitors module is set to display,
				// then show Artists & Exhibitors links
				if ( get_field('module_exhibitors', $home_page->ID ) ):
					// reset
					$link_class = '';
					// get the queried post type
					// is_post_type() throws an error
					$queried_post_type = get_query_var('post_type');

					if ( $queried_post_type == 'off_artist' ) : $link_class = 'current-menu-item'; endif;
					// artists menu item
					echo '<li class="menu-item menu-item-type-post_type_archive menu-item-object-off_artist '.$link_class.'"><a href="'.esc_url( home_url( '/artists' ) ).'">Artists</a></li> ';
					
					// reset
					$link_class = '';

					if ( $queried_post_type == 'off_exhibitor' ) : $link_class = 'current-menu-item'; endif;
					// exhibitors menu item
					echo '<li class="menu-item menu-item-type-post_type_archive menu-item-object-off_exhibitor '.$link_class.'"><a href="'.esc_url( home_url( '/exhibitors' ) ).'">Exhibitors</a></li> ';

				endif;
				
				// fetch the rest of the menu items
				$args = array(
						'theme_location' => 'main-menu',
						'container' => '',
						'depth' => 1,
						'items_wrap' => '%3$s'
					);
				wp_nav_menu( $args ); 
				
				?>
				<li></li>
			</ul>
		</div>
		<hr class="no-margin">
	</nav>

	<div id="masthead" class="wrapper">
		<div class="clear full-width" style="overflow:hidden;">
			<div id="logo" class="column ninth-4 large-text no-padding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					
					<?php 
					// different logo image for different background colors
					if ( $off_page_bg === 'dark-bg' ) {
						echo '<img class="logo-static" src="'.get_option('off_graphics_logo_dark_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_dark-bg.png').'">';
						echo '<img class="logo-hover" src="'.get_option('off_graphics_logo_dark_bg_hover', get_template_directory_uri().'/assets/images/default/154_graphics_default_dark-bg_hover.png').'">';
					} else {
						echo '<img class="logo-static" src="'.get_option('off_graphics_logo_light_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_light-bg.png').'">';
						echo '<img class="logo-hover" src="'.get_option('off_graphics_logo_light_bg_hover', get_template_directory_uri().'/assets/images/default/154_graphics_default_light-bg_hover.png').'">';
					}
					?>
					<h1>
						1:54<br>
						Contemporary<br>
						African&nbsp;Art&nbsp;Fair
					</h1>
				</a>
			</div>


			<div id="header-locations" class="column ninth-5 large-text no-padding">

				<?php 
				// "home" location
				$location_one             = get_option('off_location_link1', str_replace('1:54 ','', get_bloginfo('name')));
				$location_one_date_open   = get_option('off_location_link1_date_open');
				$location_one_date_close  = get_option('off_location_link1_date_close');
				$location_one_month_open  = get_option('off_location_link1_month_open');
				$location_one_month_close = get_option('off_location_link1_month_close');
				$location_one_year        = get_option('off_location_link1_year');
				?>
				<!-- location 1 -->
				<?php if ( $location_one ) : ?>
					<div class="location column quarter-2">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="multiple-choice active">							
						<?php 
						// location
						echo '<span class="break location-city">'.$location_one.'</span>';
						// make sure dates exist
						if ( $location_one_date_open && $location_one_date_close && $location_one_month_open ) {
							// if start month = end month
							if ( ($location_one_month_open === $location_one_month_close) || ($location_one_month_close === 'same') || ($location_one_month_close === '') ) {
								// start & end date are the same
								echo '<span class="break">'.$location_one_month_open.' <span class="keep">'.$location_one_date_open.'&ndash;'.$location_one_date_close.'</span></span>'; 
							} else {
							// end month is different from start month (ugh)
								echo '<span class="break">'.substr($location_one_month_open,0,3).'&nbsp;'.$location_one_date_open.'&ndash;'.substr($location_one_month_close,0,3).'&nbsp;'.$location_one_date_close.'</span>'; 
							}
						}
						// if no year is specified
						if ( empty($location_one_year) ) {
							// output current edition year
							echo '<span class="break"><span class="hidden location-punctuation">,&nbsp;</span>'.off_get_current_edition().'</span>';
						} else {
						// display specified year
							echo '<span class="break"><span class="hidden location-punctuation">,&nbsp;</span>'.$location_one_year.'</span>';
						}
						?>
						</a>
					</div>
				<?php endif; ?>

				<!-- location 2 -->
				<?php 
				// "away" location
				$location_two             = get_option('off_location_link2');
				$location_two_url         = '';
				$location_two_date_open   = get_option('off_location_link2_date_open');
				$location_two_date_close  = get_option('off_location_link2_date_close');
				$location_two_month_open  = get_option('off_location_link2_month_open');
				$location_two_month_close = get_option('off_location_link2_month_close');
				$location_two_year        = get_option('off_location_link2_year');

				// requires 1:54 Includes plugin
				if ( function_exists('off_get_location_url') ) {
					// check redirect preferences
					if ( off_get_location_url() ) {
						$location_two_url = esc_url( off_get_location_url() );
					}
				}
				?>
				<?php if ( $location_two ) : ?>
				<div class="location column quarter-2">
					<?php
					// url
					echo '<a href="'.esc_url($location_two_url).'" class="multiple-choice">';
					// location
					echo '<span class="break location-city">'.$location_two.'</span>';
					// make sure dates exist
					if ( $location_two_date_open && $location_two_date_close && $location_two_month_open ) {
						// if start month = end month
						if ( ($location_two_month_open === $location_two_month_close) || ($location_two_month_close === 'same') || ($location_two_month_close === '') ) {
							// start & end date are the same
							echo '<span class="break">'.$location_two_month_open.' <span class="keep">'.$location_two_date_open.'&ndash;'.$location_two_date_close.'</span></span>'; 
						} else {
						// end month is different from start month (ugh)
							echo '<span class="break">'.substr($location_two_month_open,0,3).'&nbsp;'.$location_two_date_open.'&ndash;'.substr($location_two_month_close,0,3).'&nbsp;'.$location_two_date_close.'</span>'; 
						}
					}
					// if no year is specified
					if ( empty($location_two_year) ) {
						// output current edition year
						echo '<span class="break"><span class="hidden location-punctuation">,&nbsp;</span>'.off_get_current_edition().'</span>';
					} else {
					// display specified year
						echo '<span class="break"><span class="hidden location-punctuation">,&nbsp;</span>'.$location_two_year.'</span>';
					}
					echo '</a>';
					?>
				</div>
				<?php endif; ?>

			</div>


		</div>
	</div>
	<hr class="no-margin">
</header>
