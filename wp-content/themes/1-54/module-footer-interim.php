<?php
/**
 * Footer
 *
 * Appears on all pages.
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<footer id="footer" class="page-row">
	<div id="coverall" class="white-bg relative">
		<hr class="no-margin">
		<div class="wrapper clear">

			<div class="footer-actions clear">
				<div id="search" class="relative action newsletter full-width clear">
					<?php get_search_form(); ?>
				</div>
				<div id="newsletter" class="relative action newsletter full-width light-bg clear">
					<?php include(locate_template('module-footer-mailchimp.php' )); ?>
				</div>
			</div>

			<div class="footer-info table full-width">

				<div class="table-row wide">
					<a class="table-cell quarter home-link hover-light-bg" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<div>
							1:54<br>
							Contemporary<br>
							African Art Fair
						</div>
					</a>
					<div class="table-cell quarter">
						<p>
							<?php 

							$email = is_email(get_option('off_contact_email', false));
							$email = sanitize_email($email);
							echo '<a href="mailto:'.antispambot($email,1).'">'.antispambot($email).' </a>';

							?>
							<br>
							<?php echo get_option('off_contact_phone', false); ?>
						</p>
					</div>
					<div class="table-cell quarter">
						<p>
							<?php echo get_option('off_contact_address_line1', false).'<br>'.get_option('off_contact_address_line2', false).'<br>'.get_option('off_contact_address_line3', false).'<br>'.get_option('off_contact_address_line4', false); ?>
						</p>
					</div>
					<div class="table-cell quarter">
						<div>
							<?php 
							$options = array(
								'theme_location' => 'social-menu',
								'container'	=> false,
								'depth' => 1
								);
							wp_nav_menu( $options );
							?>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</footer>