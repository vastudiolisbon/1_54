<?php
/**
 * Nav below
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) { ?>

<div class="wrapper clear">
	<nav id="nav-below" class="navigation large-text" role="navigation">
		
		<div class="column quarter-2 nav-previous">
			<div class="ninth-7">
				<?php next_posts_link(sprintf( __( '<span class="meta-nav break">&larr;</span>%s Older', 'off' ), '' ) ) ?>
			</div>
		</div>

		<div class="column quarter-2 nav-next no-padding">
			<div class="nav-next align-right">
				<?php previous_posts_link(sprintf( __( '<span class="meta-nav break">&rarr;</span>Newer %s', 'off' ), '' ) ) ?>
			</div>
		</div>

	</nav>
</div>

<?php } ?>