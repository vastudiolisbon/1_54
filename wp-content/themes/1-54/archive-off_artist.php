<?php
/**
 * Artist archive page
 *
 * Same markup as exhibitor archive page
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="module-container no-padding">
	<section>
		<?php include(locate_template('module-exhibitors-selected.php' )); ?>
	</section>

	<section class="module artists-exhibitors">
		<div class="wrapper clear">
			<header class="section-header align-center">
				<h1 class="entry-title medium-text">
					Artists 1-54 <?php echo get_bloginfo('name') . ' ' . $module_edition; ?>
				</h1> 
			</header>

			<section class="section-content clear small-text">
				<div class="list artists">
					<ul class="text-columns column-5">
				
					<?
						// if we're searching for a particular edition year ...
						if ( $off_archive_edition  ) {
							// only show artists / exhibitors from that year
							$module_edition = $off_archive_edition;
							$module_bg = 'light-bg edition-'.$module_edition;
							$query_extension = '?edition='.$module_edition; 
						} else {
						// only show the current artists / exhibitors
							$module_edition = off_get_current_edition();
						}

						$args = 'posts_per_page=-1&edition='.$module_edition.'&orderby=title&order=ASC';

						$artist_query = new WP_Query( $args . '&post_type=off_artist' );

						$current_letter = '';

						while ( $artist_query->have_posts() ) : $artist_query->the_post() ?>
					
						<?php
						$this_letter = strtoupper(substr(get_the_title(),0,1));
						if ( $this_letter != $current_letter ) {
							if ($this_letter != 'A') {
								echo '</ul></li>';
							}
							echo '<li><h2 class="medium-text">'.$this_letter.'</h2><ul>';
							$current_letter = $this_letter;
						}
						// if artist has thumbnail
						if ( has_post_thumbnail() ) {
							// get the info
							$thumb_id = get_post_thumbnail_id();
							$thumb_url = wp_get_attachment_image_src( $thumb_id, 'off_thumb_artist_hover', false );
							// build an img to store on the artist link
							$hover_content = '<img src=\''.$thumb_url[0].'\'/>';

						} else {
						// nothing to show
							$hover_content = '';
						}

						// $hover_content = '';

						?>

						<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="image" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

					<?php endwhile; ?>
				
					</ul>

				</div>
						
				<?php wp_reset_query(); ?>
			</section>
		</div>
	</section>
</section>

<?php include(locate_template('footer.php' )); ?>