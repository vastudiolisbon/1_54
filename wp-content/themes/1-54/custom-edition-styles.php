<?php
/**
 * Edition Styles
 *
 * Custum colors for different fair editions 
 * 
 * @package WordPress
 * @subpackage 1:54
 */

function generate_edition_styles() {

// current edition CSS
$edition_current_css = <<<EOTAAA

a:hover
, a:active
, .dark-bg a:hover
, .dark-bg a:active
, .dark-bg .white-bg a:hover
, .dark-bg .white-bg a:active
, .dark-bg .opposite-bg a:hover
, .dark-bg .opposite-bg a:active
, .white-bg a:hover
, .white-bg a:active
, .white-bg .dark-bg a:hover
, .white-bg .dark-bg a:active
, .white-bg .opposite-bg a:hover
, .white-bg .opposite-bg a:active
, .slide a:hover p
, .artist-representation:hover
, .artist-representation:active
, .white-bg .artist-representation:hover
, .white-bg .artist-representation:active {
  color: %master_color_light%; 
}


.light-bg
, .light-bg #nothing-found input
, .light-bg .page-details input
, .light-bg .post-password-form input
, .dark-bg .light-bg
, .white-bg .light-bg
, .entry-content h3:before
, .entry-content a[href*="eventbrite"]
, .hover-light-bg:hover
, .page-details
, .post-password-form
, .multiple-choice:hover:before
, .multiple-choice:active:before
, .multiple-choice.active:before
, #action-button
, #nothing-found
, #menu a:hover
, #menu a:active
, #menu .current-menu-item a
, #menu .current_page_item a
, input:focus
, input:hover
, a:hover .thumbnail {
  background-color: %master_color_light%;
}

.module.forum .forum-block a:hover {
	border-color: %master_color_light%;
}

.dark-bg
, .white-bg .dark-bg
, .white-bg .opposite-bg {
  background-color: %master_color_dark%;
}

@media only screen and (max-width : 900px) {
	#header-locations a.multiple-choice.active
	, #header-locations a.multiple-choice:hover {
		background-color: %master_color_light%;
	}
}

@media only screen and (max-width : 775px) {
	#search #s:focus
	, #search:hover #s:focus {
		background-color: %master_color_light%;
	}
}

EOTAAA;

// archived editions css
$edition_past_css = <<<EOTAAA

.light-bg.edition-%edition_slug%
, .light-bg.edition-%edition_slug% .hover-light-bg:hover
, .light-bg.edition-%edition_slug% a:hover .thumbnail
, .dark-bg .light-bg.edition-%edition_slug%
, .dark-bg .light-bg.edition-%edition_slug% .archive-menu a:hover
, .dark-bg .light-bg.edition-%edition_slug% .hover-light-bg:hover
, .dark-bg .light-bg.edition-%edition_slug% a:hover .thumbnail
, .dark-bg .light-bg.edition-%edition_slug% .hover-light-bg:hover
, .dark-bg .light-bg.edition-%edition_slug% a:hover .thumbnail
, .white-bg .light-bg.edition-%edition_slug%
, .white-bg .light-bg.edition-%edition_slug% .hover-light-bg:hover
, .white-bg .light-bg.edition-%edition_slug% a:hover .thumbnail {
	background-color: %archive_color_light%; 
}

.light-bg.edition-%edition_slug%.edition-link:hover
, .dark-bg .light-bg.edition-%edition_slug%.edition-link:hover
, .white-bg .light-bg.edition-%edition_slug%.edition-link:hover {
	background-color: #1a1a1a;
	color: %archive_color_light%;
}

.edition-archive.light-bg .dark-bg
, .edition-archive.light-bg .opposite-bg {
	background-color: transparent;
	color: #1a1a1a; 
}

EOTAAA;

// get all edition info
$args = array(
	'hide_empty' => false,
	'orderby' => 'name',
	'order' => 'DESC'
	);

$editions = get_terms('edition', $args);


$search_master = array(
	'%master_color_light%',
	'%master_color_dark%'
	);

$search_archive = array(
	'%archive_color_light%',
	'%edition_slug%'
	);

$output = '';

foreach ( $editions as $edition ) {

	$edition_id = $edition->term_taxonomy_id;
	$edition_slug = $edition->slug;
	$edition_meta = get_option("taxonomy_term_$edition_id");
	$edition_color_light = $edition_meta['color_light'];
	$edition_color_dark = $edition_meta['color_dark'];

	$master_color_light = '';
	$master_color_dark = '';
	$archive_color_light = '';

	// if it's the current edition
	if ( $edition->slug === off_get_current_edition() ) {

		$master_color_light = $edition_color_light;
		$master_color_dark = $edition_color_dark;

		$replace = array(
			$master_color_light
			, $master_color_dark
		);

		// generate the master edition styles
		$output .= str_replace($search_master, $replace, $edition_current_css);

	} else {
	// it's an archived edition

		$archive_color_light = $edition_color_light;

		$replace = array(
			$archive_color_light
			, $edition_slug
		);

		// generate archived edition styles
		$output .= str_replace($search_archive, $replace, $edition_past_css);

	}

}

$location_prefix = get_option('off_location_prefix');
if ( empty($location_prefix) ) { 
	$location_prefix = 'default'; 
}

// write to file
$file = dirname(__FILE__).'/assets/css/154_edition-styles_'.$location_prefix.'.css';

file_put_contents( $file, $output ); 

}

add_action('create_edition', 'generate_edition_styles');
add_action('edit_edition', 'generate_edition_styles');
add_action('delete_edition', 'generate_edition_styles');

?>