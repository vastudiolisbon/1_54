<?php
/**
 * Ads Module
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php 

$args = array(
	'posts_per_page' => 2,
	'post_type' => 'off_adverts',
	// only show artists / exhibitors who have thumbnails
	'meta_key'    => '_thumbnail_id',
	'orderby' => 'rand'
);

$module_query = new WP_Query( $args ); if ( $module_query->have_posts() ) : ?>

<div class="sponsors column ninth-2 no-padding float-right">

<?php while ( $module_query->have_posts() ) : $module_query->the_post(); ?>

	<div class="sponsor small-text float-right thumbnail">
		<a href="<?php echo get_field('advert_url'); ?>" target="_blank">
			<?php echo get_the_post_thumbnail( $post->ID, 'off_adverts_2x' ); ?> 
		</a>
		<div class="clear"></div>
	</div>

<?php endwhile; ?>

</div>

<?php endif; wp_reset_query(); ?>