<?php
/**
 * Mailchimp Signup
 *
 * Appears on all pages. Does not use MC CSS; style is incorporated into template, assumes below markup.
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>

<div id="signup-blind" class="absolute align-center full-width full-height text-tiny-caps light-bg">
	SIGN UP FOR OUR NEWSLETTER
</div>

<form action="http://1-54.us4.list-manage.com/subscribe/post?u=a9bf64987f771c3c15ddd0ccc&amp;id=fbe41a761c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
	
	<div class="mc-field-group">
		
		<label class="screen-reader-text hidden" for="mce-EMAIL">Email Address</label>
		<input type="email" value="" name="EMAIL" class="required email quarter no-padding full-height align-center float-left" id="mce-EMAIL" placeholder="EMAIL">

		<label class="screen-reader-text hidden" for="mce-FNAME">First Name</label>
		<input type="text" value="" name="FNAME" class="quarter no-padding full-height border-left align-center float-left" id="mce-FNAME" placeholder="FIRST NAME">
		
		<label class="screen-reader-text hidden" for="mce-LNAME">Last Name</label>
		<input type="text" value="" name="LNAME" class="quarter no-padding full-height border-left align-center float-left" id="mce-LNAME" placeholder="LAST NAME">
	</div>

	<!-- <div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>  -->
	<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	<div style="position: absolute; left: -5000px;"><input type="text" name="b_a9bf64987f771c3c15ddd0ccc_fbe41a761c" tabindex="-1" value=""></div>
	
	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="quarter no-padding full-height align-center border-left float-right white-bg">

</form>