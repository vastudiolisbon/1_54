<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="clear">
		
		<label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
		
		<input class="align-center float-left primary button" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="SEARCH" />
		
		<input class="align-center float-right light-bg quarter secondary" type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'SUBMIT', 'submit button' ); ?>" />
	
	</div>
</form>