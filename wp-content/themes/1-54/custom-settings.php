<?php

// Create custom settings page in the admin
function register_off_custom_settings_page() {
	add_submenu_page( 
		'options-general.php',
		'Color & Graphics', 
		'Color & Graphics', 
		'manage_network_options',
		'off_custom-settings',
		'edit_off_custom_settings' 
	);
}
add_action( 'admin_menu', 'register_off_custom_settings_page' );

function edit_off_custom_settings() {
	
	?>

	<div class="wrap">

		<form method="post" action="options.php">
			<?php wp_nonce_field( 'update-options' ) ?>

			<h2>Color &amp; Graphics Settings</h2>

			<h3>Location Prefix</h3>
			<p class="description">
				The location prefix is used to link to all the necessary graphics and assets used by the 1:54 website template. This prefix should correspond to the graphics file names located in this theme's graphics folder. If a location prefix is not provided, the default theme graphics will be used. 
			</p>
			<p>
				For example: 154_graphics_london_light-bg.png. In this example, <code>london</code> is the location prefix.
			</p>
			<p>
				<input type="text" class="regular-text" name="off_location_prefix" placeholder="london" value="<?php echo get_option('off_location_prefix'); ?>" />
			</p>

			<h3>Menu Color</h3>
			<p class="description">
				Set the top navigation menu background color scheme:
			</p>

			<p>
				<fieldset>
					<label>
						<input type="radio" name="off_menu_color" value="white-bg" <?php if ( get_option('off_menu_color') == 'white-bg' ){ echo 'checked="checked"'; } ?> />
						<span>White</span>
					</label>
					<br>
					<label>
						<input type="radio" name="off_menu_color" value="dark-bg" <?php if ( get_option('off_menu_color') == 'dark-bg' ){ echo 'checked="checked"'; } ?> />
						<span>Current fair edition dark background color</span>
					</label>
				</fieldset>
			</p>
			
			<p class="submit">
				<input type="submit" name="submit" class="button button-primary" value="Save Changes" />
			</p>

			<p>&nbsp;</p>
			<hr>

			<h3>About the Location Prefix</h3>
			<p>
				Each time a new site is created on the 1:54 network, a set of graphics specific to that location must be placed inside a folder of the same name, and added to the theme's <code>images</code> folder. An Illustrator template for these graphics is located within the theme's <code>designs</code> folder. Both of these folders must be accessed via FTP. If you do not know how to do this, please get in touch with the site's administrator.
			</p>
						
			<p>&nbsp;</p>
			<hr>
			
			<h3>File Locations</h3>

			<p>
				The <b>design file</b> used to create the location graphics is located inside the 1-54 theme directory: <code><?php echo get_template_directory_uri(); ?>/assets/designs"></code>.
			</p>
			<p>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/154_graphics-settings_01.png" style="zoom:0.5;">
			</p>
			<p>
				The <b>graphics</b> used on the site are located inside the 1-54 theme directory, under their respective location prefix: <code><?php echo get_template_directory_uri(); ?>/assets/images/"></code>.
			</p>
			<p>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/154_graphics-settings_02.png" style="zoom:0.5;">
			</p>

			<p>&nbsp;</p>
			<hr>
			
			<h3>Creating New Graphics</h3>

			<p>
				To create graphics for a new location:
			</p>
			<ol>
				<li>
					make a copy of <em>154_graphics_default.ai</em>, and change "default" to your new location name. For example: 154_graphics_johannesburg.ai.
				</li>
				<li>
					Change the colors and graphics, if necessary. The forum background pattern can be any scale, as long as it is a repeat pattern, however <b>do not change the file sizes of the logos</b>.
				</li>
				<li>
					Export PNGs using the following settings:
					<p>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/154_graphics-settings_03.png" style="zoom:0.5;">
					</p>
					<p>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/154_graphics-settings_04.png" style="zoom:0.5;">
					</p>
					Make sure you've turned off the "guides" layer.<br>
					<p>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/admin/154_graphics-settings_05.png" style="zoom:0.5;">
					</p>
				</li>
				<li>
					Place all exported PNGs inside a folder using the same location prefix, then upload that folder to this theme's <code>images</code> folder via FTP.
				</li>
				<li>
					Update the location prefix above.
				</li>
			</ol>

			<p>&nbsp;</p>
			<hr>

			<h3>Graphics Currently in Use</h3>
			
			<?php 
			// get location prefix
			$location_prefix = get_option('off_location_prefix');
			// if it's empty, use default
			if ( empty($location_prefix) ) {
				$location_prefix = 'default';
			}

			?>
			<p>
				<code>154_graphics_<?php echo $location_prefix; ?>_favicon-32.png</code>
				<br>
				<br>
				<img style="width:16px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $location_prefix; ?>/154_graphics_<?php echo $location_prefix; ?>_favicon-32.png">
				<br>
				<br>
			</p>
			<p>
				<code>154_graphics_<?php echo $location_prefix; ?>_light-bg.png</code>
				<br>
				<br>
				<img style="max-width:200px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $location_prefix; ?>/154_graphics_<?php echo $location_prefix; ?>_light-bg.png">
				<br>
				<br>
			</p>
			<p>
				<code>154_graphics_<?php echo $location_prefix; ?>_dark-bg.png</code>
				<br>
				<br>
				<img style="max-width:200px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $location_prefix; ?>/154_graphics_<?php echo $location_prefix; ?>_dark-bg.png">
				<br>
				<br>
			</p>
			<p>
				<code>154_graphics_<?php echo $location_prefix; ?>_forum-bg.png</code>
				<br>
				<br>
				<img style="max-width:200px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $location_prefix; ?>/154_graphics_<?php echo $location_prefix; ?>_forum-bg.png">
				<br>
				<br>
			</p>
			<p>
				<code>154_graphics_<?php echo $location_prefix; ?>_forum-bg@2x.png</code>
				<br>
				<br>
				<img style="max-width:200px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/<?php echo $location_prefix; ?>/154_graphics_<?php echo $location_prefix; ?>_forum-bg@2x.png">
				<br>
				<br>
			</p>

			<input type="hidden" name="action" value="update" />
			<input type="hidden" name="page_options" value="off_location_prefix,off_menu_color" />
		</form>
	</div>

<?php } ?>