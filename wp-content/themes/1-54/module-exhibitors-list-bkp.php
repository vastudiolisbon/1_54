<?php
/**
 * Artist & Exhibitors list
 *
 * Used on the home page, Artist & Exhibitor single and archive pages
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php 

global $off_current_edition;
global $off_archive_edition;
global $off_page_bg;

$module_display_first = '';
$module_edition = '';

$thumb_url = '';
$hover_content = '';
$hover_content_type = '';

$query_extension = '';

// display artists or exhibitors first?
if ( get_query_var('post_type') ) {
	$module_display_first = get_query_var('post_type');
} else {
// default to artists (it's probably the home page)
	$module_display_first = 'off_artist';
}

// different rules for the home page
if ( !is_front_page() && !is_tax() ) { 
	$module_bg = 'white-bg'; 
// } elseif ( is_tax() ) { 
	// $module_bg = 'light-bg';
} else {
	$module_bg = 'opposite-bg';
}

// if we're searching for a particular edition year ...
if ( $off_archive_edition  ) {
	// only show artists / exhibitors from that year
	$module_edition = $off_archive_edition;
	$module_bg = 'light-bg edition-'.$module_edition;
	$query_extension = '?edition='.$module_edition; 
} else {
// only show the current artists / exhibitors
	$module_edition = off_get_current_edition();
}

$args = 'posts_per_page=-1&edition='.$module_edition.'&orderby=title&order=ASC';

$artist_query = new WP_Query( $args . '&post_type=off_artist' );

$exhibitor_query = new WP_Query( $args . '&post_type=off_exhibitor' );

if ( $artist_query->have_posts() && $exhibitor_query->have_posts() ) :

	$current_letter = '';

?>
<section class="module artists-exhibitors <?php echo $module_bg; ?>">

	<div class="wrapper clear">

		<header class="section-header clear">
			<h1 class="entry-title large-text column">
				<a href="#" class="multiple-choice <?php if($module_display_first=='off_artist'){echo 'active';} ?>" data-choice="artists">Artists</a>
			</h1>
			<h1 class="entry-title large-text column">
				<a href="#" class="multiple-choice <?php if($module_display_first=='off_exhibitor'){echo 'active';} ?>" data-choice="exhibitors">Exhibitors</a>
			</h1>
		</header>

		<section class="section-content clear small-text">

		<!-- artist list -->
		<div class="list choice artists <?php if($module_display_first=='off_artist'){echo 'active';} ?>">

			<ul class="text-columns column-5">
		
			<? while ( $artist_query->have_posts() ) : $artist_query->the_post() ?>
			
				<?php
				$this_letter = strtoupper(substr(get_the_title(),0,1));
				if ( $this_letter != $current_letter ) {
					if ($this_letter != 'A') {
						echo '</ul></li>';
					}
					echo '<li><h2 class="large-text">'.$this_letter.'</h2><ul>';
					$current_letter = $this_letter;
				}
				// if artist has thumbnail
				if ( has_post_thumbnail() ) {
					// get the info
					$thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src( $thumb_id, 'off_thumb_artist_hover', false );
					// build an img to store on the artist link
					$hover_content = '<img src=\''.$thumb_url[0].'\'/>';

				} else {
				// nothing to show
					$hover_content = '';
				}

				// $hover_content = '';

				?>

				<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="image" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

			<?php endwhile; ?>
		
			</ul>

		</div>
		
		<?php wp_reset_query(); ?>
		
		<!-- exhibitor list -->
		<div class="list choice exhibitors <?php if($module_display_first=='off_exhibitor'){echo 'active';} ?>">

			<ul class="text-columns column-4">
		
			<?php while ( $exhibitor_query->have_posts() ) : $exhibitor_query->the_post(); ?>
				
				<?php
				// show hover content
				if (get_field('exhibitor_meta_info')) {
					$hover_content = substr(get_field('exhibitor_meta_info'),0,40);
				} else {
				// nothing to show
					$hover_content = '';
				}

				?>
				<li><a href="<?php echo get_the_permalink().$query_extension; ?>" data-hover-type="text" data-hover-content="<?php echo $hover_content; ?>"><?php the_title(); ?></a></li>

			<?php endwhile; ?>
			
			</ul>

		</div>
		
		<?php wp_reset_query(); ?>
		
		</section>

	</div>
</section>

<?php endif; ?>