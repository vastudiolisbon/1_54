<?php
/**
 * Exhibitor archive module
 *
 * Primary content for both artist & exhibitor archive pages.
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php
// artists or exhibitors?
$module_post_type = get_query_var('post_type');
// if we're searching for a particular edition year ...
if ( get_query_var('term') ) {
	// only show artists / exhibitors from that year
	$module_edition = get_query_var('term');
} else {
// only show the current artists / exhibitors
	$module_edition = $off_current_edition;
}
?>
<!-- <div class="dark-bg">  -->
	<div class="header wrapper clear">

		<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo admin_url('edit.php?post_type='.$module_post_type); ?>" class="post-edit-link">EDIT <?php echo strtoupper(get_post_type_object($module_post_type)->labels->name); ?></a>
		<?php endif; ?>
	</div>
<!-- </div> -->
<section>
	
	<?php include(locate_template('module-exhibitors-selected.php' )); ?>

	<?php include(locate_template('module-exhibitors-list.php' )); ?>

</section>