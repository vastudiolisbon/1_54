<?php
/**
 * Archive nav
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php

global $off_archive_edition;

$query_extension = '';
$edition_link    = '';

// if it's an edition archive page
if ( is_tax() ) {
	// get the edition from the URL
	$off_archive_edition = get_query_var('edition');
}

$query_extension = '?edition='.$off_archive_edition;
$edition_link    = get_term_link( $off_archive_edition, 'edition' );

// get all pages tagged with the archived edition
$args = array(
	'post_type' => 'page',
	'edition' => $off_archive_edition,
	'orderby' => 'title',
	'order' => 'ASC'
);

$page_query = new WP_Query( $args );

?>
<div class="header archive">

	<div class="wrapper clear">
	
		<header class="page-header">
			<h1 class="entry-title medium-text">
				<a href="<?php echo esc_url( get_permalink( get_page_by_path( 'archive' ) ) ); ?>"><?php echo get_bloginfo('name'); ?></a> <?php echo '<a href="'.$edition_link.'">' .  $off_archive_edition . '</a>'; ?>
			</h1> 

			<?php if ( $page_query->have_posts() ) : ?> 

			<nav class="archive-menu">
				<ul class="menu clear">
				<?php while ( $page_query->have_posts() ) : $page_query->the_post(); ?>
					<li class="serif"><a href="<?php echo get_the_permalink().$query_extension; ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
				</ul>
			</nav>

			<?php endif; wp_reset_query(); ?>

		</header>

	</div>
<hr>
</div>