<?php
/**
 * Circular Link
 *
 * NBD.
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php
// url
$button_url = get_field('action_button_url', $post->ID);
// external URL: true or false
$button_external = get_field('action_button_target', $post->ID);
// button text
$button_title = get_field('action_button_text', $post->ID);
// open in a new window?
if ( $button_external ) { $target = 'target="_blank"'; }
// build the button
echo '<div id="action-button" class="light-bg text-tiny-caps fixed align-center"><a href="'.$button_url.'" '.$target.' class="hover-inverse">'.strtoupper($button_title).'</a></div>';
?>