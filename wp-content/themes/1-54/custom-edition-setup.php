<?php
/**
 * Edition Info
 *
 * Set up edition graphics, styles, colors, etcetra.
 * 
 * @package WordPress
 * @subpackage 1:54
 */


$graphics_location = '';
$graphics_prefix   = '';
$menu_bg           = '';
$current_edition   = '';

// get location prefix
$graphics_location = get_option('off_location_prefix');

// if location is undefined ...
if ( empty($graphics_location) ) { 
	// use default
	$graphics_location = 'default'; 
}

// code assumes filenames work like this: images/[location]/154_graphics_[location]_filename.png
$graphics_prefix = $graphics_location.'/154_graphics_'.$graphics_location.'_';

// menu background color
$menu_bg = get_option('off_menu_color');

// current edition
$current_edition = get_term_by( 'slug', get_option( 'off_current_edition', 'one' ), 'edition' );

// echo '<!-- ^^^^^^^^^^ '.PHP_EOL;
// echo '-- EDITION --'.PHP_EOL;
// print_r($edition);
// echo ' ^^^^^^^^^^ -->'.PHP_EOL;

// echo PHP_EOL;

// echo '<!-- ^^^^^^^^^^ '.PHP_EOL;
// echo '-- CURRENT EDITION --'.PHP_EOL;
// print_r($current_edition);
// echo ' ^^^^^^^^^^ -->';
?>