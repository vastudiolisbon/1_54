<?php
/**
 * Search Results
 * 
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main">

<?php if ( have_posts() ) : ?>
<!-- Search results -->

<div class="wrapper">

	<header class="page-header quarter-2">
		<h1 class="entry-title medium-text"><?php printf( __( 'Search Results for: %s', 'off' ), get_search_query() ); ?></h1>
	</header>

</div>
<section class="returned-results">

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?> ">
			<hr>
			
			<div class="wrapper">

				<?php edit_post_link(); ?>

				<a href="<?php the_permalink(); ?>" rel="bookmark" class="clear">

					<div class="column ninth-2">
					<?php if ( has_post_thumbnail() ) {
						echo '<figure class="thumbnail">';
							the_post_thumbnail('off_thumb_artist_hover_2x');
						echo '</figure>';
					} else {
						echo '&nbsp;';
					}
					?>
					</div>
					<div class="ninth-5 center no-padding">

						<header class="entry-header">
							<?php 

							if ( $post->post_type === 'post' ) {
								echo '<span class="text-tiny-caps">News</span>';
							}
							if ( $post->post_type === 'off_artist' ) {
								echo '<span class="text-tiny-caps">Artist</span>';
							}
							if ( $post->post_type === 'off_exhibitor' ) {
								echo '<span class="text-tiny-caps">Exhibitor</span>';
							}

							?>

							<h2 class="large-text">
								<?php the_title(); ?>
							</h2>
						</header>

						<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>

					</div>

				</a>

			</div>

		</article>

	<?php endwhile; ?>

	<?php get_template_part( 'nav', 'below' ); ?>

</section>

<?php else : ?>
<!-- No search results -->
<div class="wrapper">

	<article id="post-0" class="post no-results not-found">

		<header class="page-header quarter-2">
			<h1 class="entry-title x-large-text"><?php _e( 'Not Found', 'off' ); ?></h1>
		</header>

		<section class="entry-content ninth-5 large-text no-padding">
			<div id="nothing-found" class="clear">
				<p class="large-text"><?php _e( 'Sorry, nothing matched your search. Please try again.', 'off' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</section>

	</article>

</div> <!-- /.wrapper -->

<?php endif; ?>

<?php get_footer(); ?>