<!-- action button -->
<?
// only show content if page is not password protected
if ( !post_password_required() ) {
	// if ( get_field('show_action_button' ) ) {
	// 	include(locate_template('module-action-button.php' ));
	// }
}
?>
<div class="clear"></div>

</main> <!-- .page-row .page-row-expanded -->

<?php //include(locate_template('module-footer.php' )); ?>

<div id="footer">
	<div class="wrapper">
		<div class="social">
			<p class="newsletter-link"><a href="#">Sign up to our Newsletter</a></p>

			<?php
				$options = array(
					'theme_location' => 'social-menu',
					'container'	=> false,
					'depth' => 1
					);

				wp_nav_menu( $options );
			?>
		</div>

		<div class="sponsors-ads">
			<a href="/privacy-policy">Privacy Policy</a>
			<?php
				while(the_repeater_field('ad_sponsor_partner', 'option')) {
					$img = wp_get_attachment_image_src(get_sub_field('image_logo', 'option'), 'large')[0];
			?>

				<a href="<?php echo get_sub_field('link', 'option'); ?>">
					<img src="<?php echo $img; ?>"/>
				</a>

			<?php
				}
			?>
		</div>
	</div>
</div>

<div class="newsletter-dialog">
	<div>
		<h2 class="">Sign up to our Newsletter</h2>

		<form method="post" action="" id="subscribe_newsletter">
			<input type="text" name="first_name" placeholder="First Name" required="required" />
			<input type="text" name="last_name" placeholder="Last Name" required="required" />
			<input type="email" name="email" placeholder="E-mail" required="required" />

			<style>
				.newsletter_messages {
					color: #666;
					display: block;
					float: left;
					width: 100%;
					padding: 10px 10px;
					border-top: 1px solid;
					border-left: 1px solid #000;
					border-right: 1px solid #000;
				}

				.newsletter_messages small {
					font-size: 12px;
					line-height: 16px;
					display: inline-block;
				}

				.newsletter_messages a {
					color: #666;
					text-decoration: underline;
				}

				.newsletter_messages input {
					display: inline-block;
					float: none;
					margin-right: 10px;
					width: auto;
				}

				.newsletter_messages p {
					display: inline-block;
					margin-bottom: 10px;
				}
			</style>

			<?php
				$siteid = get_current_blog_id();
				switch_to_blog(1);
			?>
			<div class="newsletter_messages">
				<p>
					<input type="checkbox" name="optin-newsletter" value="" id="optin-newsletter" required style="-webkit-appearance: checkbox !important; appearance: checkbox !important;"/>
					<label for="optin-newsletter">
						<?php
							$newsletter_text_1 = get_field('newsletter_text_1', 'option');
							$newsletter_text_1 = str_replace('<p>', '', $newsletter_text_1);
							$newsletter_text_1 = str_replace('</p>', '', $newsletter_text_1);
							echo $newsletter_text_1;
						?>
					</label>
				</p>

				<p>
					<input type="checkbox" name="optin-terms" value="" id="optin-terms" required style="-webkit-appearance: checkbox !important; appearance: checkbox !important;"/>
					<label for="optin-terms">
						<?php
							$newsletter_text_2 = get_field('newsletter_text_2', 'option');
							$newsletter_text_2 = str_replace('<p>', '', $newsletter_text_2);
							$newsletter_text_2 = str_replace('</p>', '', $newsletter_text_2);
							echo $newsletter_text_2;
						?>
						<br>
						<small>
							<?php
								$newsletter_text_3 = get_field('newsletter_text_3', 'option');
								$newsletter_text_3 = str_replace('<p>', '', $newsletter_text_3);
								$newsletter_text_3 = str_replace('</p>', '', $newsletter_text_3);
								echo $newsletter_text_3;
							?>
						</small>
					</label>
				</p>
			</div>

			<?php
				switch_to_blog($siteid);
			?>

			<input type="button" name="" value="Dismiss" class="dismiss" />
			<input type="submit" name="" placeholder="Subscribe" />
		</form>

		<div class="message"></div>
	</div>
</div>

<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51907769-1', '1-54.com');
  ga('send', 'pageview');

</script>

</body>
</html>
