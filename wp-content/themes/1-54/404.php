<?php $off_page_bg = 'light-bg'; ?>

<?php include(locate_template('header.php')); ?>

<section id="content" role="main">

	<div class="wrapper clear">
					
		<header class="page-header quarter-2">
			<!-- <h1 class="entry-title x-large-text"><?php _e( 'Not Found', 'off' ); ?></h1> -->
		</header>

		<section class="entry-content ninth-5 large-text no-padding">

			<div id="nothing-found" class="clear">
				<p class="large-text"><?php _e( 'Nothing found for the requested page. Try a search instead?', 'off' ); ?></p>
				<?php get_search_form(); ?>
			</div>

		</section>

	</div>

</section>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>