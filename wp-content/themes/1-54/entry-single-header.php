<?php

/**
 * Entry Header
 * 
 * @package WordPress
 * @subpackage 1:54
 */

?>

<?php 

global $off_current_edition;
global $off_archive_edition;

$module_edition = '';
$query_extension = '';

$post_title = get_the_title();

// if the page is password protected ...
if ( post_password_required() ) {
	// title can appear as "Protected: ... "
	echo $post_title;

} else {
	// remove protected
	echo str_replace('Protected: ','', $post_title ); 

	if ( $off_archive_edition || get_query_var('edition') ) {
		if ( get_query_var('edition') ) {
			$module_edition = get_query_var('edition');
		} else {
			$module_edition = $off_archive_edition;
		}
		$query_extension = '?edition='.$module_edition;
	} else {
		if ( $off_current_edition ) {
			$module_edition = $off_current_edition;
		}
	}

	// single artist pages
	if ( get_post_type() == 'off_artist' ) {
		// find connected galleries
		$connected = new WP_Query( array(
			'connected_type' => 'represented',
			'connected_items' => get_queried_object(),
			'nopaging' => true,
			'edition' => $module_edition
		) );
		// display them
		if ( $connected->have_posts() ) :
		
			while ( $connected->have_posts() ) : $connected->the_post(); 

				echo '<span class="break small-text"><a class="artist-representation" href="'.get_the_permalink().$query_extension.'">'.get_the_title().'</a></span>';
			
			endwhile; wp_reset_postdata();

		endif;

	}

}

?>