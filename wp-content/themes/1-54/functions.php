<?php
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Theme setup
----------------------------------------------- */

// GO!
function off_setup() {

	// add theme support
	add_theme_support( 'html5', array( 'search-form', 'caption' ) );
	add_theme_support( 'post-thumbnails' );

	// ads
	add_image_size('off_adverts', 140, 140, true);
	add_image_size('off_adverts_2x', 280, 280, true);
	// news archive
	add_image_size('off_thumb', 300, 200, true);
	add_image_size('off_thumb_2x', 600, 400, true);
	// selected artists/exhibitors
	add_image_size('off_thumb_exhibitor', 400, 400, false);
	add_image_size('off_thumb_exhibitor_2x', 800, 800, false);
	// artist list hover thumbnail
	add_image_size('off_thumb_artist_hover', 200, 200, false);
	add_image_size('off_thumb_artist_hover_2x', 400, 400, false);
	// feature carousel
	add_image_size('off_carousel', 525, 350, false);
	add_image_size('off_carousel_2x', 1050, 700, false);

	// post type support
	add_post_type_support( 'page', 'excerpt' );
	add_post_type_support( 'post', 'excerpt' );
	add_post_type_support( 'off_artist', 'excerpt' );
	add_post_type_support( 'off_exhibitor', 'excerpt' );

	// disable content width
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = '';

	// nav menus
	register_nav_menus( array(
		  'main-menu' => __( 'Main Menu', 'off' ),
		  'social-menu' => __( 'Footer Social Menu', 'off' )
		)
	);
}
add_action( 'after_setup_theme', 'off_setup' );

add_filter( 'the_title', 'off_title' );
function off_title( $title ) {
	if ( $title == '' ) {
		// title separator
		return '';
	} else {
		return $title;
	}
}


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Customize & Extend
----------------------------------------------- */

// but only on the admin side ...
if ( is_user_logged_in() ) :
	// Load custom admin options
	include 'custom-admin.php';

endif;


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Edition-specific Colors & Settings
----------------------------------------------- */

// current fair edition (i.e., 2014, 2015)
global $off_current_edition;
// if we're looking at an archive(d) page
global $off_archive_edition;
// abbreviation or acronym for the current site install (i.e., london or ny), used to specify graphics files
global $off_location_prefix;
// directory and file prefix for all graphics, used in combination with location prefix
global $off_graphics_prefix;
// white or edition dark color
global $off_menu_bg;
// page background colors vary
global $off_page_bg;

// CURRENT EDITION
// returns the slug of the current edition
if ( function_exists('off_get_current_edition') ) {
	$off_current_edition = off_get_current_edition();
} else {
	$off_current_edition = '';
}


// LOCATION & STYLES
// if location prefix has been defined
if ( get_option('off_location_prefix') ) {
	// use it
	$off_location_prefix = get_option('off_location_prefix');
	// if current edition has been set, load edition styles
	if ( get_option( 'off_current_edition', false ) ) {
		include 'custom-edition-styles.php';
	}

} else {
// use default settings
	$off_location_prefix = 'default';
}

// SPECIFY GRAPHIC SET
// code assumes structure: ".../[location]/154_graphics_[location]_filename.png"
$off_graphics_prefix = get_template_directory_uri().'/assets/images/'.$off_location_prefix.'/154_graphics_'.$off_location_prefix.'_';

// MENU BACKGROUND
// if a choice has been made
if ( get_option('off_menu_color') ) {
	// use it
	$off_menu_bg = get_option('off_menu_color');

} else {
// white is default
	$off_menu_bg = 'white-bg';
}



/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Scripts & Stylesheets
----------------------------------------------- */

function off_register_scripts() {

	$script_filepath = get_template_directory_uri().'/assets/js';

	/* Vendor JS
	-------------------- */

	//extra plugins
	wp_register_script('plugins_js', $script_filepath.'/vendor/plugins.js', array('jquery'), '1.3.13', true );

	// slideshows
	wp_register_script('slick_js', $script_filepath.'/vendor/slick/slick.js', array('jquery'), '1.3.13', true );

	// slideshows
	wp_register_script('bx_js', $script_filepath.'/vendor/jquery.bxslider/jquery.bxslider.min.js', array('jquery'), '4.1.2', true );

	// responsive video embeds
	// wp_register_script('fitvids_js', $script_filepath.'/vendor/jquery.fitvids.js', array('jquery'), '1.1', true );


	/* 1:54 JS
	-------------------- */

	// video
	wp_register_script('off_video_js', $script_filepath.'/154.video.js', array('jquery'), '1.1', true );

	// main
	wp_register_script('off_main_js', $script_filepath.'/154.main.js', array('jquery','plugins_js'), '1.1', true );
	wp_register_script('off_main_js', $script_filepath.'/154.main.js', array('jquery'), '1.1', true );

	// home
	wp_register_script('off_home_js', $script_filepath.'/154.home.js', array('jquery','slick_js'), '1.1', true );

	// single
	wp_register_script('off_single_js', $script_filepath.'/154.single.js', array('jquery','off_main_js','off_video_js'), '1.1', true );

	// all pages
	wp_enqueue_script('off_main_js');
	wp_enqueue_script('off_single_js');
	wp_enqueue_script('off_home_js');
}

add_action('wp_enqueue_scripts','off_register_scripts');


// LOCATION STYLES
function off_enqueue_css() {

	$style_filepath = get_template_directory_uri().'/assets/';

	$location = get_option('off_location_prefix');

	if ( empty($location) || $location === null ) {
		$location = 'default';
	}

	wp_register_style('off_theme_styles', $style_filepath.'scss/154_style.css?v3', false, false, 'screen' );

	wp_enqueue_style('off_theme_styles');

	if ( file_exists(dirname(__FILE__). '/assets/css/154_edition-styles_'.$location.'.css') ) {

		$version = date("U", filemtime(dirname(__FILE__). '/assets/css/154_edition-styles_'.$location.'.css'));

		wp_register_style('edition_styles', $style_filepath.'css/154_edition-styles_'.$location.'.css', false, $version, 'all' );
		wp_enqueue_style('edition_styles');

	}

}
add_action('wp_print_styles', 'off_enqueue_css');



/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Functions, Hooks, Filters, ETC
----------------------------------------------- */

// SITE TITLE
function off_filter_wp_title( $title ) {
	// display name instead of URL
	return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_filter( 'wp_title', 'off_filter_wp_title' );


// EXCERPT
// length, in words
function off_excerpt_length( $length ) {
	// 50 words
	return 50;
}
// "more" text
function off_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_length', 'off_excerpt_length', 999 );
add_filter( 'excerpt_more', 'off_excerpt_more' );


// POST CONNECTIONS
// Assumes P2P Plugin
function off_connection_types() {
	// check that plugin is installed
	if (function_exists('p2p_register_connection_type')) {
		// create a new connection type between artists and exhibitors
		p2p_register_connection_type( array(
			'name' => 'represented',
			'from' => 'off_artist',
			'to' => 'off_exhibitor',
			'reciprocal' => true,
			'title' => 'Artist Representation'
		) );
	}
}
add_action( 'p2p_init', 'off_connection_types' );


// SOCIAL LINKS (footer)
// Limit number of nav menu items on menu
// http://wordpress.stackexchange.com/questions/109569/limit-top-level-menu-items-on-wp-nav-menu
function off_nav_social_chop( $sorted_menu_items, $args ) {
	// check that menu exists
	if ( $args->theme_location != 'social-menu' )
		return $sorted_menu_items;
	// reset
	$unset_top_level_menu_item_ids = array();
	$array_unset_value = 1;
	$count = 1;

	// get all menu items
	foreach ( $sorted_menu_items as $sorted_menu_item ) {
		// unset top level menu items if > 4
		if ( 0 == $sorted_menu_item->menu_item_parent ) {
			if ( $count > 5 ) {
				unset( $sorted_menu_items[$array_unset_value] );
				$unset_top_level_menu_item_ids[] = $sorted_menu_item->ID;
			}
			$count++;
		}
		// unset child menu items of unset top level menu items
		if ( in_array( $sorted_menu_item->menu_item_parent, $unset_top_level_menu_item_ids ) )
		unset( $sorted_menu_items[$array_unset_value] );

		$array_unset_value++;
	}
	// output the chopped menu
	return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'off_nav_social_chop', 10, 2 );


// WYSIWIG EDITOR
// add custom stylesheet
function off_editor_styles() {
	// locate style sheet
	$editor_styles = get_template_directory_uri().'/assets/scss/wp_editor-styles.css';
	// include that shit.
	add_editor_style( $editor_styles );
}
add_action( 'admin_init', 'off_editor_styles' );



// THUMBNAIL CAPTION
// can't actually remember why i included this ...
// https://wordpress.org/support/topic/displaying-featured-image-caption
function the_post_thumbnail_description() {

  global $post;
  $thumb_id = get_post_thumbnail_id($post->id);

  $args = array(
		'post_type' => 'attachment',
		'post_status' => null,
		'post_parent' => $post->ID,
		'include' => $thumb_id
	);
	// retrieve the thumbnail
   $thumbnail_image = get_posts($args);
   // if thumbnail exists
   if ($thumbnail_image && isset($thumbnail_image[0])) {
     // output the caption
     echo '<figcaption class="wp-caption-text">'.$thumbnail_image[0]->post_excerpt.'</figcaption>';
     // thumbnail description
     // echo $thumbnail_image[0]->post_content;
  }
}


// GENERATE AN EXCERPT
// http://stackoverflow.com/questions/965235/how-can-i-truncate-a-string-to-the-first-20-words-in-php
function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
}


// ADD ANCHORS TO HEADER TAGS
// https://github.com/cferdinandi/add-ids-to-header-tags-plus/blob/master/add-ids-to-header-tags-plus-process.php
function add_anchors_to_headers( $content ) {

	if ( ! is_singular() ) {
		return $content;
	}

	$pattern = '#(?P<full_tag><(?P<tag_name>h\d)(?P<tag_extra>[^>]*)>(?P<tag_contents>[^<]*)</h\d>)#i';

	if ( preg_match_all( $pattern, $content, $matches, PREG_SET_ORDER ) ) {

		$find = array();
		$replace = array();

		foreach( $matches as $match ) {

			if ( strlen( $match['tag_extra'] ) && false !== stripos( $match['tag_extra'], 'id=' ) ) {
				continue;
			}

			$find[]    = $match['full_tag'];
			$id        = sanitize_title( $match['tag_contents'] );
			$id_attr   = sprintf( ' id="%s"', $id );

			$extra     = sprintf( '<a id="%s" class="section-anchor absolute full-width"></a>', $id );
			$replace[] = sprintf( '<%1$s%2$s>%3$s%4$s</%1$s>', $match['tag_name'], $match['tag_extra'], $match['tag_contents'], $extra );

		}

		$content = str_replace( $find, $replace, $content );

	}

	return $content;

}
add_filter( 'the_content', 'add_anchors_to_headers' );



/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Shortcodes
----------------------------------------------- */

/**
* [off_map]
* Example: [map address=”Strand London WC2R 1LA United Kingdom”]
* creates a static google map
* @param {string} address. Map address.
* @param {integer} zoom. Zoom level of the map. Default is 14.
*/

function off_map( $attributes, $content = null ) {

	// define some defaults
	$defaults = shortcode_atts( array(
		'address' => 'Strand London WC2R 1LA United Kingdom',
		'zoom' => 14
	), $attributes );

	// get specified address
	$map_address = $defaults['address'];
	// make it URL safe
	$url_address = str_replace(' ', '+', $map_address);
	// get specified zoom
	$map_zoom = $defaults['zoom'];

	$map_marker = get_template_directory_uri().'/assets/images/154_graphics_map-marker.png';

	$output = '';

	// generate the map image link
	$output = '<a href="http://www.google.com/maps/place/'.$url_address.'" target="_blank"><figure class="thumbnail"><img src="http://maps.googleapis.com/maps/api/staticmap?center='.$url_address.'&amp;scale=2&amp;markers=scale:2|icon:'.$map_marker.'%7C'.$url_address.'&amp;zoom='.$map_zoom.'&amp;size=700x450&amp;format=pngstyle=feature%3Aall%7Celement%3Ageometry%7Cvisibility%3Asimplified%7C&amp;style=feature%3Aadministrative%7Celement%3Aall%7Cvisibility%3Aoff%7C&amp;style=feature%3Apoi%7Celement%3Aall%7Cvisibility%3Aoff%7C&amp;style=feature%3Atransit%7Celement%3Ageometry%7Cvisibility%3Aoff%7C&amp;style=feature%3Atransit%7Celement%3Alabels%7Csaturation%3A-100%7Cvisibility%3Asimplified%7C&amp;style=feature%3Aroad%7Celement%3Aall%7Csaturation%3A-100%7Clightness%3A5%7Cvisibility%3Asimplified%7C&amp;style=feature%3Awater%7Celement%3Alabels%7Cvisibility%3Aoff%7C&amp;style=feature%3Awater%7Celement%3Ageometry%7Ccolor%3A0xC8C8C8%7C&amp;style=feature%3Alandscape%7Celement%3Ageometry%7Ccolor%3A0xF0F0F0%7C"></figure></a>';

	return $output;

}
add_shortcode('off_map', 'off_map');




/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Get rid of some WordPress BS
----------------------------------------------- */

// DISABLE RSS
// disable RSS feed, point to homepage instead
function fb_disable_feed() {
	wp_die( __('No feed available, please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);


// "DISABLE" WP GALLERY
// by removing the "Add Gallery" button. gallery shortcodes will still work
add_action( 'admin_footer-post-new.php', 'disable_media_gallery_wpse_76095' );
add_action( 'admin_footer-post.php', 'disable_media_gallery_wpse_76095' );
function disable_media_gallery_wpse_76095() {
	?>
	<script type="text/javascript">
		jQuery(document).ready( function($) {
			$(document.body).one( 'click', '.insert-media', function( event ) {
			   $(".media-menu").find("a:contains('Gallery')").remove();
			});
		});
	</script>
	<?php
}


if(function_exists('acf_add_options_page') && get_current_blog_id() == 1) {

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}

$option_page = acf_add_options_page(array(
	'page_title' 	=> 'Ads/Sponsors',
	'menu_title' 	=> 'Ads/Sponsors',
	'menu_slug' 	=> 'ads-sponsors',
	'capability' 	=> 'edit_posts',
	'redirect' 	=> false
));

//ALLOW SVG UPLOAD
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
  global $wp_version;
  if ( $wp_version !== '4.7.1' ) {
     return $data;
  }

  $filetype = wp_check_filetype( $filename, $mimes );

  return [
      'ext'             => $filetype['ext'],
      'type'            => $filetype['type'],
      'proper_filename' => $data['proper_filename']
  ];

}, 10, 4 );

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );


function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );




////

@ini_set( 'upload_max_size' , '2048M' );
@ini_set( 'post_max_size', '2048M');
@ini_set( 'max_execution_time', '10000' );

///

//AJAX
function CCAjax() {
	//print_r($_POST);

	$post_request = [
	    'api_key' => '2843cdd485eb4a73bcde425d0bb28fb9', # supplied separately - never display in your HTML!
	    'firstname' => $_POST['first_name'], # mandatory
	    'lastname' => $_POST['last_name'],
	    'email' => $_POST['email'],
	    'categories' => '16'
	];

	$response = wp_remote_post( 'https://app.artlogic.net/154artfair/public/api/mailings/signup', array(
		'method' => 'POST',
		'timeout' => 45,
		'redirection' => 5,
		'httpversion' => '1.0',
		'blocking' => true,
		'headers' => array(),
		'body' => $post_request,
		'cookies' => array()
	    )
	);

	if ( is_wp_error( $response ) ) {
	   $error_message = $response->get_error_message();
	   echo "Something went wrong: $error_message";
	} else {
	   echo $response['body'];
	}

	/*if($_POST['content']=='material'){
    	include get_template_directory() . '/includes/material-conteudo.php';
   	}

   	else if($_POST['content']=='materials_load'){
    	include get_template_directory() . '/includes/materials-load.php';
   	}*/

	exit();
}

// creating Ajax call for WordPress
add_action('wp_ajax_nopriv_CCAjax', 'CCAjax');
add_action('wp_ajax_CCAjax', 'CCAjax');
