<?php
/**
 * Single Post (News)
 *
 * Uses the current edition's dark color as the background color.
 *
 * @package WordPress
 * @subpackage 1:54
 */
?>
<?php $off_page_bg = 'dark-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="activate-news-link">

	<?php get_template_part( 'entry','page' ); ?>

	<?php get_template_part( 'nav','below-single' ); ?>

</section>

<?php include(locate_template('footer.php' )); ?>