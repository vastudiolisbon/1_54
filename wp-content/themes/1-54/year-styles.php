<style type="text/css">
	<?php
		switch_to_blog(1);
		$yearColor = get_field('site_color', 'option');
		restore_current_blog();
	?>

	a:hover, a:active {
		color: <?php echo $yearColor; ?>;
		opacity: 1;
	}

	#header .cities-nav .wrapper > ul > li.active, #header .cities-nav .wrapper > ul > li:hover {
		border-bottom: 2px solid <?php echo $yearColor; ?>;
	}

	#header .cities-nav .wrapper > ul > li:hover a {
		color: <?php echo $yearColor; ?>;
	}

	#header .cities-nav .wrapper > ul > li.active a {
		color: <?php echo $yearColor; ?>;
	}

	#header .main-nav ul li.current-menu-item a {
		color: <?php echo $yearColor; ?>;
	}

	#header .secondary-nav ul li.current-menu-item a {
		color: <?php echo $yearColor; ?>;
	}

	#header .main-nav ul li.search-wrapper form input[type='text'] {
		border-color: <?php echo $yearColor; ?>;
	}

	#header .main-nav ul li.search-wrapper form input[type='text']::-webkit-input-placeholder { /* Chrome/Opera/Safari */
		color: <?php echo $yearColor; ?>;
	    font-size: 1.1rem;
	}
	#header .main-nav ul li.search-wrapper form input[type='text']::-moz-placeholder { /* Firefox 19+ */
		color: <?php echo $yearColor; ?>;
	    font-size: 1.1rem;
	}
	#header .main-nav ul li.search-wrapper form input[type='text']:-ms-input-placeholder { /* IE 10+ */
		color: <?php echo $yearColor; ?>;
	    font-size: 1.1rem;
	}
	#header .main-nav ul li.search-wrapper form input[type='text']:-moz-placeholder { /* Firefox 18- */
		color: <?php echo $yearColor; ?>;
	    font-size: 1.1rem;
	}

	.entry-content a {
		color: #000;
	}

	.entry-content a:hover {
		color: <?php echo $yearColor; ?>;
	}

	.multiple-choice:hover, .multiple-choice:active, .multiple-choice.active {
		color: <?php echo $yearColor; ?>;
		border-color: <?php echo $yearColor; ?>;
	}

	.list.galleries ul li a:hover, .list.special-projects ul li a:hover {
		color: <?php echo $yearColor; ?>;
	}

	.hover.preview {
		color: <?php echo $yearColor; ?>;
		border-color: <?php echo $yearColor; ?>;
	}

	#header .main-nav .logo:hover svg, #header .main-nav .logo:hover svg * {
		fill: <?php echo $yearColor; ?>;
	}

	/* .read-more a:hover {
		color: <?php echo $yearColor; ?> !important;
	} */

	.artist-representation:hover {
		color: <?php echo $yearColor; ?> !important;
	}
</style>