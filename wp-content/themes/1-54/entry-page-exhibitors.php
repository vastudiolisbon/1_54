<?php

/**
 * Artists & Exhibitors page Module
 *
 * Used on all single artist & exhibitor templates.
 *
 * @package WordPress
 * @subpackage 1:54
 */

global $off_current_edition;
global $off_archive_edition;

$module_edition = '';
$query_extension = '';

?>
<div class="wrapper clear">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<header class="page-header align-center ninth-5">
		<h1 class="medium-text">
			<?php get_template_part( 'entry','single-header' ); ?>
		</h1>

		<div class="small-bio">
			<?php
				// meta info
				if ( get_post_type() == 'off_artist' ) {
					if (get_field('exhibitor_meta_info')) {
						the_field('exhibitor_meta_info');
					}
				}

				// single exhibitor pages
				if ( get_post_type() == 'off_exhibitor' ) {
					// find connected artists
					$connected = new WP_Query( array(
						'connected_type' => 'represented',
						'connected_items' => get_queried_object(),
						'nopaging' => true,
						'orderby' => 'title',
						'order' => 'ASC',
						'edition' => $module_edition
					) );
					// display them
					if ( $connected->have_posts() ) :

						echo '<p style="margin-bottom:0;">Presenting Artist(s):</p><ul class="presented-artists">';

						while ( $connected->have_posts() ) : $connected->the_post();



							echo '<li><a href="'.get_the_permalink().$query_extension.'">'.get_the_title().'</a></li>';

						endwhile; wp_reset_postdata();

						echo '</ul>';

					endif;
				}
			?>
		</div>
	</header>

	<section class="ninth-5 no-padding center">
		<?php edit_post_link(); ?>
	</section>

	<section class="entry-content ninth-5 serif no-padding">

	<?php

	get_template_part( 'entry','thumbnail' );

	the_content();

	?>

	</section>

	<section class="exhibitor entry-content ninth-5">

		<?php

		if ( $off_archive_edition || get_query_var('edition') ) {
			if ( get_query_var('edition') ) {
				$module_edition = get_query_var('edition');
			} else {
				$module_edition = $off_archive_edition;
			}
			$query_extension = '?edition='.$module_edition;
		} else {
			if ( $off_current_edition ) {
				$module_edition = $off_current_edition;
			}
		}

		?>
	</section>

<?php endwhile; endif; ?>

</div>
