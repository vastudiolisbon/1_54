<?php
/**
 * 404
 *
 * Redirects to home page
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>
<?php
	$home = esc_url( home_url( '/' ) );
	header("Status: 301 Moved Permanently");
	header("Location:".$home);
?>