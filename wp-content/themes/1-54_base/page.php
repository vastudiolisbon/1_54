<?php
/**
 * Default Page Template
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main">
	<?php get_template_part( 'entry','page' ); ?>
</section>

<?php include(locate_template('footer.php' )); ?>
