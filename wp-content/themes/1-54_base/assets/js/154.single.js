jQuery(document).ready(function($){

	var lastRead
		// get saved place from URL
		, link = window.location.hash.replace('/','').toString()
		, linkLocation = link.replace('#','')
		// elements to generate anchors for
		, createAnchors = $('.entry-content h1, .entry-content h3');
		;

	createAnchors.each(function(index){

		$(this).prepend('<a id="anchor_'+index+'" class="section-anchor absolute full-width">');

		if ( $('body').hasClass('logged-in') ) {
			$(this).append('<div class="anchor-details" style="padding:0 0 40px;"><div class="sans light-bg shadowed">'+window.location.href.replace(window.location.hash, '')+'#'+index+'</div></div>');
		}

	});

	$(window).load(function(){
		if ( linkLocation.length > 0 ) {
			setTimeout(function(){
				$('html, body').animate({
					scrollTop: $('#anchor_'+linkLocation).offset().top
				}, 0);
			}, 100 );
		}

	});

});