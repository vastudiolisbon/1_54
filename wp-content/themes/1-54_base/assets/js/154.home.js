jQuery(document).ready(function($){

	var _directory = $('body').data('directory')
		, _winWidth = $('body').attr('data-win-width')
		, _winHeight = $('body').attr('data-win-height')
		, _slickStyles = '<link rel="stylesheet" href="'+_directory+'/assets/js/vendor/slick/slick.css">'
		;	

	$(_slickStyles).insertBefore('head link[rel="stylesheet"]:first');


	$(window).load(function(){

		$('#slideshow-tray').slick({
			dots: false
			, arrows: true
			, appendArrows: $('#slideshow-tray')
			, prevArrow: '<div class="nav prev absolute align-left"><div class="arrow absolute"></div></div>'
			, nextArrow: '<div class="nav next absolute align-right"><div class="arrow absolute"></div></div>'
			, infinite: true
			, swipeToSlide: true
			, variableWidth: true
			, variableHeight: true
			, lazyLoad: 'ondemand'
			, respondTo: 'min'
		});

		$('#slideshow-container').fadeIn(300);

	});

	$('.artists-exhibitors .multiple-choice').each(function(){

		$(this).bind({

			click : function(e) {

				e.preventDefault();

				var choice = $(this).attr('data-choice')

				$('.artists-exhibitors .multiple-choice').removeClass('active').parents().find('.choice').removeClass('active');

				$(this).addClass('active').parents().find('.choice.'+choice).addClass('active');

			}

		});

	})


	$('.artists-exhibitors .list a').each(function(){

		$(this).bind({

			mouseenter : function() {
					// text or image
				var hoverType = $(this).attr('data-hover-type')
					// content to display
					, hoverContent = $(this).attr('data-hover-content')
					// containing element
					, hoverContainer = $(this).parent('li').find('.hover.preview')
					// max width of container, set via CSS
					, hoverMaxWidth = 250 
					// x, y offset from mouse position
					, hoverOffsetX = 20
					, hoverOffsetY = 30
					// mouse position
					, mouseX = event.clientX
					, mouseY = event.clientY
					// hover element position
					, hoverX = mouseX + hoverOffsetX
					, hoverY = mouseY + hoverOffsetY
					// window dimensions
					, _winWidth = $('body').attr('data-win-width')
					, _winHeight = $('body').attr('data-win-height')
					;

				if ( hoverContent.length ) {
					
					// if hoverContainer doesn't exist yet
					if ( hoverContainer.length < 1 ) {
						// build it
						hoverContainer = $('<div class="'+hoverType+' hover preview shadowed light-bg fixed large-text">'+hoverContent+'</div>');
						// add it to the dom
						$(this).parent('li').append( hoverContainer );
					}

					// if hover preview might be displayed offscreen
					if ( (hoverX + hoverMaxWidth) >= _winWidth - hoverOffsetX ) {
						// put it on the left side of the mouse
						hoverX = mouseX - hoverOffsetX - hoverMaxWidth;
						// re. height: user can scroll up / down to correct
					}
					// position and display the hover preview
					hoverContainer.css({
						'left' : hoverX + 'px',
						'top' : hoverY + 'px',
						'display' : 'block'
					});

				 }

			},
			mouseleave : function() {
				// hide the preview
				$(this).parent('li').find('.hover.preview').delay(30).fadeOut(100);
			}

		});

	});


});