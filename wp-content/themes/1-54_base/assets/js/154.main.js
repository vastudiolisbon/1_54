/*

Loaded to all pages

*/

jQuery(document).ready(function($){ 

	// page scroll position
	var _scrollPos = $(window).scrollTop()
		// window height
		, _winHeight = $(window).height()
		// window width
		, _winWidth = $(window).width()
		// document height
		, _docHeight = $(document).height()
		// fixed nav height (effects scroll bottom position)
		, _headerHeight = $('#header nav').height()
		// footer height
		, _footerHeight = $('#footer').height()
		// button element (only one per page)
		, _actionButton = $('.action-button')
		// button height
		, _actionButtonHeight = $('#action-button').height()
		// set via stylesheet, button is fixed 40px from the bottom of the window
		, _actionButtonBottom = 40
		// position of scroll at window's bottom edge
		, _scrollBottom = _scrollPos + _headerHeight + _winHeight
		;

	// delete multiple instances of button
	if ( _actionButton.length > 0 ) {
		_actionButton.each(function(index){
			if ( index > 0 ) {
				$(this).remove();
			}
		});
	} else {
		_actionButton = $('#action-button');
	}

	$(window).bind({

		load : function(){
			// document height
			_docHeight = $(document).height()
			watchButton();
			updateWinDimensions();
			animateNav();
		},
		scroll : function(){
			// temporarily, until responsive styles are in place
			// sets left scroll to 0px
			$(this).scrollLeft(0); 
			setTimeout(function(){
				watchButton();
				animateNav();
			}, 30 );
		},
		resize : function(){
			setTimeout(function(){
				watchButton();
				updateWinDimensions();
			}, 30 );
		}

	});

	function updateWinDimensions() {
		// update dimensions
		_winHeight = $(window).height()
		, _winWidth = $(window).width()
		, _docHeight = $(document).height()
		;
		// store them on the doc `body` so other scripts can use this info
		$('body').attr('data-win-width', _winWidth ).attr('data-win-height', _winHeight ).attr('data-doc-height', _docHeight);

	}

	function animateNav() {

		_docHeight = $(document).height()
		, _scrollPos = $(window).scrollTop()
		;

		if ( _docHeight > 500 ) {

			if ( _scrollPos >= 200 ) {

				if( !$('body').hasClass('scrolled') ) {
					$('#menu').css('top','-150px').animate({
						top : 0
					}, 100 );
					$('body').addClass('scrolled');
				}

			} else {
				$('body').removeClass('scrolled');
			}
		} else {
			$('body').removeClass('scrolled');
		}
	}

	// offset button from backgrounds of the same color
	function	watchButton() {
		// if _actionButton exists
		if ( _actionButton.length ) {

			// update scroll positions
			_scrollPos = $(window).scrollTop()
			, _winHeight = $(window).height()
			, _scrollBottom = _scrollPos + _headerHeight + _winHeight
			;

			$('.go-behind-button').each(function(){
				// top position of the background module
				bgModuleTop = $(this).offset().top
				// bottom position of the background module
				, bgModuleBottom = bgModuleTop + $(this).height()
				;

				// if the top of the background module reaches the bottom of the button ...
				if ( (bgModuleTop + _actionButtonBottom*2) <= _scrollBottom && 
					// ... and the bottom of the background module reaches above the middle of the botton
					(bgModuleBottom + _actionButtonBottom + _actionButtonHeight*1.5 ) >= _scrollBottom ) {

					// offset the button
					if ( !_actionButton.hasClass('shadowed') ) {
						_actionButton.addClass('shadowed');
					}
				} else {
				// flat button is fine
					_actionButton.removeClass('shadowed');
				}
			});

		}
	} // watchButton()	

});