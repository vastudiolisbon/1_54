<?php
/**
 * Header
 *
 * Used everywhere, of course. 
 * @param $background_class is passed from the referring template to set the page's background color.
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */

// requires 1:54 Base Includes plugin
if ( function_exists('off_get_redirect_url') ) {
	// check redirect preferences
	if ( off_get_redirect_url() ) {
		$destination_url = esc_url( off_get_redirect_url() );
		// send visitor elsewhere
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $destination_url");
		exit;
	}
}

?>
<!DOCTYPE html> 
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<link rel="icon" type="image/png" href="<?php echo $off_graphics_prefix; ?>favicon-32.png">

	<title><?php wp_title( '&mdash;', true, 'right' ); ?></title>

	<link href="http://fnt.webink.com/wfs/webink.css/?project=AFED3C06-E8E9-4E5C-9665-08D341207DD5&fonts=BA766C3D-9F83-4950-AFCD-AD9F2BF5CEAB:f=Theinhardt-Regular,F77BBDE3-5270-5846-90AD-5529C2FFDA57:f=Theinhardt-Medium" rel="stylesheet" type="text/css"/>

	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?> data-directory="<?php echo get_template_directory_uri(); ?>" data-mobile="<?php if(wp_is_mobile()){echo'true';}else{echo'false';} ?>">

	<?php 
	// using include so we can pass variables between templates
	include(locate_template('module-header.php' )); ?>

	<main class="page-row page-row-expanded <?php echo $off_page_bg; ?>">