<?php
/**
 * Header Nav
 *
 * Appears on all pages, has two styles contingent upon options page selection: dark home page or light home page.
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>

<header id="header" class="page-row <?php if ( $off_page_bg === 'dark-bg' ) { echo $off_page_bg; } ?>">
	
	<?php 
	// different logo image for different background colors
	if ( $off_page_bg === 'dark-bg' ) {
		$img_color = $off_page_bg;
	} else {
		$img_color = 'light-bg';
	}
	?>

	<div id="masthead" class="wrapper">
		<div class="clear full-width" style="overflow:hidden;">
			<div id="logo" class="column ninth-4 large-text no-padding">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo $off_graphics_prefix.$img_color.'.png'; ?>">
					<h1>
						1:54<br>
						Contemporary<br>
						African&nbsp;Art&nbsp;Fair
					</h1>
				</a>
			</div>
		</div>
	</div>
	<hr class="no-margin">
</header>
