<?php
/**
 * Custom Admin settings
 *
 * Remove/change a bunch of WP defaults from admin interface
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */

// Remove comments, WP logo, and pages from top admin menu
function wps_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('comments');
	$wp_admin_bar->remove_menu('new-page');
	$wp_admin_bar->remove_menu('themes');
	$wp_admin_bar->remove_menu('customize');
	$wp_admin_bar->remove_menu('menus');
}
add_action('wp_before_admin_bar_render','wps_admin_bar');

// 'Logged in as' instead of 'Howdy'
function replace_howdy($wp_admin_bar) {
	$my_account = $wp_admin_bar->get_node('my-account');
	$newtitle = str_replace('Howdy,','Logged in as', $my_account->title);
	$wp_admin_bar->add_node(array(
		'id'=>'my-account',
		'title'=>$newtitle,
		));
}
add_filter('admin_bar_menu','replace_howdy', 25);

// Remove comments ...
// from Pages
function remove_pages_count_columns($defaults) {
	unset($defaults['comments']);
	return $defaults;
}
add_filter('manage_pages_columns', 'remove_pages_count_columns');
// from Posts
function remove_posts_count_columns($defaults) {
	unset($defaults['comments']);
	return $defaults;
}
add_filter('manage_posts_columns', 'remove_posts_count_columns');



// documentation widget for display on dashboard
// function off_add_dashboard_widget() {
// 	wp_add_dashboard_widget(
// 		'documentation_widget', // slug
// 		'Site Documentation', // title
// 		'documentation_widget_function' // display function
//  );
// }
// add_action( 'wp_dashboard_setup', 'off_add_dashboard_widget' );

// display the documentation widget
// function documentation_widget_function() {
// 	echo '<style type="text/css">#documentation_widget { background-color: #ffff80; border: 1px solid rgb(240, 220, 100); } #documentation_widget .hndle, #documentation_widget .stuffbox .hndle { border-bottom: 1px solid rgb(240, 220, 100); }</style>';
// 	echo "For tips and help on creating and editing content, please refer to the <a href='#' target='_blank'>site documentation</a>.";
// }


// rename 'Posts' to something else
function revcon_change_post_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'News';
	$submenu['edit.php'][5][0]  = 'News';
	$submenu['edit.php'][10][0] = 'Add News';
	$submenu['edit.php'][16][0] = 'News Tags';
	echo '';
}

function revcon_change_post_object() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels -> name               = 'News';
	$labels -> singular_name      = 'News';
	$labels -> add_new            = 'Add News';
	$labels -> add_new_item       = 'Add News';
	$labels -> edit_item          = 'Edit News';
	$labels -> new_item           = 'News';
	$labels -> view_item          = 'View News';
	$labels -> search_items       = 'Search News';
	$labels -> not_found          = 'No News found';
	$labels -> not_found_in_trash = 'No News found in Trash';
	$labels -> all_items          = 'All News';
	$labels -> menu_name          = 'News';
	$labels -> name_admin_bar     = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );




