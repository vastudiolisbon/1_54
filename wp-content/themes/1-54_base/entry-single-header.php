<?php
/**
 * Entry Header
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>
<?php 

$post_title = get_the_title();

// if the page is password protected ...
if ( post_password_required() ) {
	// title can appear as "Protected: ... "
	echo $post_title;

} else {
	// remove protected
	echo str_replace('Protected: ','', $post_title ); 

}

?>