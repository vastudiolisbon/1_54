<?php
/**
 * Page Module
 *
 * Used on all single page templates
 * 
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>

<div class="wrapper clear">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<header class="page-header quarter-2">
		<h1 class="entry-title x-large-text">
			<?php get_template_part( 'entry','single-header' ); ?>
		</h1> 
	</header>

	<section class="ninth-5 no-padding center">
		<?php edit_post_link(); ?>
	</section>

	<section class="entry-content ninth-5 serif no-padding">
		<?php the_content(); ?>
	</section>

<?php endwhile; endif; ?>

</div>