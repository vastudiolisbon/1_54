<?php
/**
 * Index Page
 * 
 * Default, shows all recent posts.
 *
 * @package WordPress
 * @subpackage 1:54 Base
 */
?>
<?php $off_page_bg = 'white-bg'; ?>
<?php include(locate_template('header.php' )); ?>

<section id="content" role="main" class="news-archive">

	<div class="wrapper">

		<header class="page-header quarter-2">
			<h1 class="entry-title x-large-text">News</h1> 
		</header>

		<section class="clear">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'entry' ); ?>

		<?php endwhile; endif; ?>

		</section>

	</div>

</section>

<?php include(locate_template('footer.php' )); ?>