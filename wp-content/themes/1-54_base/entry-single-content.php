<section class="entry-content ninth-5 serif no-padding">

	<?php 
		
	edit_post_link();
	
	get_template_part( 'entry','thumbnail' );

	the_content(); 

	?>

</section>