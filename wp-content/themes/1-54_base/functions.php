<?php
/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Theme setup
----------------------------------------------- */

// GO!
function off_setup() {

	// add theme support
	add_theme_support( 'html5', array( 'search-form', 'caption' ) );
	add_theme_support( 'post-thumbnails' );

	// disable content width
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = '';

	// nav menus
	register_nav_menus( array( 
		  'main-menu' => __( 'Main Menu', 'off' ), 
		  'social-menu' => __( 'Footer Social Menu', 'off' )
		)
	);
}
add_action( 'after_setup_theme', 'off_setup' );

add_filter( 'the_title', 'off_title' );
function off_title( $title ) {
	if ( $title == '' ) {
		// title separator
		return '';
	} else {
		return $title;
	}
}


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Customize & Extend
----------------------------------------------- */

// but only on the admin side ... 
if ( is_user_logged_in() ) :
	// Load custom admin options
	include 'custom-admin.php';
	// Load custom settings
	// include 'custom-settings.php';
endif;


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Edition-specific Colors & Settings
----------------------------------------------- */

// current fair edition (i.e., 2014, 2015)
global $off_current_edition;
// abbreviation or acronym for the current site install (i.e., london or ny), used to specify graphics files
global $off_location_prefix;
// directory and file prefix for all graphics, used in combination with location prefix
global $off_graphics_prefix;
// white or edition dark color
global $off_menu_bg;
// page background colors vary
global $off_page_bg;

// CURRENT EDITION OBJECT
if ( function_exists( off_get_current_edition_object() ) ) {
	$off_current_edition = off_get_current_edition_object();
}

// LOCATION & STYLES
// if location prefix has been defined
if ( get_option('off_location_prefix') ) {
	// use it
	$off_location_prefix = get_option('off_location_prefix');
	// if current edition has been set, load edition styles
	if ( get_option( 'off_current_edition', '' ) ) {
		include 'custom-edition-styles.php';
	}

} else {
// use default settings
	$off_location_prefix = 'default';
}

// SPECIFY GRAPHIC SET
// code assumes structure: ".../[location]/154_graphics_[location]_filename.png"
$off_graphics_prefix = get_template_directory_uri().'/assets/images/'.$off_location_prefix.'/154_graphics_'.$off_location_prefix.'_';

// MENU BACKGROUND
// if a choice has been made
if ( get_option('off_menu_color') ) {
	// use it
	$off_menu_bg = get_option('off_menu_color');

} else {
// white is default
	$off_menu_bg = 'white-bg';
}


/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Scripts & Stylesheets
----------------------------------------------- */

// LOCATION STYLES
function off_enqueue_css() {

	$style_filepath = get_template_directory_uri().'/assets/';

	$location = get_option('off_location_prefix');

	if ( empty($location) || $location === null ) { 
		$location = 'default'; 
	}

	wp_register_style('off_theme_styles', $style_filepath.'scss/154_style.css', false, false, 'screen' );

	wp_enqueue_style('off_theme_styles');

	if ( file_exists(dirname(__FILE__). '/assets/css/154_edition-styles_'.$location.'.css') ) {

		$version = date("U", filemtime(dirname(__FILE__). '/assets/css/154_edition-styles_'.$location.'.css'));

		wp_register_style('edition_styles', $style_filepath.'css/154_edition-styles_'.$location.'.css', false, $version, 'all' );
		wp_enqueue_style('edition_styles');
		
	}

}
add_action('wp_print_styles', 'off_enqueue_css');



/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Functions, Hooks, Filters, ETC
----------------------------------------------- */

// SITE TITLE
function off_filter_wp_title( $title ) {
	// display name instead of URL
	return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_filter( 'wp_title', 'off_filter_wp_title' );


// SOCIAL LINKS (footer)
// Limit number of nav menu items on menu
// http://wordpress.stackexchange.com/questions/109569/limit-top-level-menu-items-on-wp-nav-menu
function off_nav_social_chop( $sorted_menu_items, $args ) {
	// check that menu exists
	if ( $args->theme_location != 'social-menu' )
		return $sorted_menu_items;
	// reset
	$unset_top_level_menu_item_ids = array();
	$array_unset_value = 1;
	$count = 1;
	
	// get all menu items
	foreach ( $sorted_menu_items as $sorted_menu_item ) {
		// unset top level menu items if > 4
		if ( 0 == $sorted_menu_item->menu_item_parent ) {
			if ( $count > 4 ) {
				unset( $sorted_menu_items[$array_unset_value] );
				$unset_top_level_menu_item_ids[] = $sorted_menu_item->ID;
			}
			$count++;
		}
		// unset child menu items of unset top level menu items
		if ( in_array( $sorted_menu_item->menu_item_parent, $unset_top_level_menu_item_ids ) )
		unset( $sorted_menu_items[$array_unset_value] );

		$array_unset_value++;
	}
	// output the chopped menu
	return $sorted_menu_items;
}
add_filter( 'wp_nav_menu_objects', 'off_nav_social_chop', 10, 2 );


// WYSIWIG EDITOR
// add custom stylesheet
function off_editor_styles() {
	// locate style sheet
	$editor_styles = get_template_directory_uri().'/assets/scss/wp_editor-styles.css';
	// include that shit.
	add_editor_style( $editor_styles );
}
add_action( 'admin_init', 'off_editor_styles' );



/* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	Get rid of some WordPress BS 
----------------------------------------------- */

// DISABLE RSS
// disable RSS feed, point to homepage instead
function fb_disable_feed() {
	wp_die( __('No feed available, please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);


// "DISABLE" WP GALLERY
// by removing the "Add Gallery" button. gallery shortcodes will still work
add_action( 'admin_footer-post-new.php', 'disable_media_gallery_wpse_76095' );
add_action( 'admin_footer-post.php', 'disable_media_gallery_wpse_76095' );
function disable_media_gallery_wpse_76095() 
{
	?>
	<script type="text/javascript">
		jQuery(document).ready( function($) {
			$(document.body).one( 'click', '.insert-media', function( event ) {
			   $(".media-menu").find("a:contains('Gallery')").remove();
			});
		});
	</script>
	<?php
}

