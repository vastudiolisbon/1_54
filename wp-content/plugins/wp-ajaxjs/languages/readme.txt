Translations have moved to
https://translate.wordpress.org/

Thank you for your contribution.

Plugin Name: wp-ajaxjs
Description: Most leading CMS platforms like WordPress use Ajax in their architecture.
Version: 5.02
Author: Alex Meton
Author URI: http://wordpress.com
License: GPL2
