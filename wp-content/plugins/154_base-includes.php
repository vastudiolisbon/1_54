<?php

/**
 * @package 1:54 Includes
 * @version 1.0
 */

/*

Plugin Name: 1:54 Base Includes
Description: Enable redirect options for 1:54 multisite network.
Author: TH
Version: 1.0

*/


function off_redirect_settings_api_init() {
	// redirect settings section
	add_settings_section(
			'off_redirect_settings_section',
			'Redirect Settings',
			'off_redirect_settings_section_function',
			'general'
		);
	// redirect setting field
	add_settings_field(
			'off_redirect',
			'Redirect visitors to',
			'off_redirect_function',
			'general',
			'off_redirect_settings_section'
		);

	register_setting( 'general', 'off_redirect' );

	function off_redirect_settings_section_function() {
		echo '<p>Choose which site you want to make active. When visitors head to <a href="'.esc_url( home_url( '/' ) ).'" target="_blank">'.get_site_url().'</a>, they will be immediately redirected to the active site.</p>';

	}
	// display the option select
	function off_redirect_function() {
		
		$args = array(
				'network_id' => get_current_site()->id,
				'public' => true,
				'deleted' => false,
				'offset' => 0
			);
		// get the full site list
		$blog_list = wp_get_sites($args);
		// get this site's blog id
		$this_blog_id = get_current_blog_id();
		// has a selection already been made?
		$off_redirect = get_option('off_redirect','');

		$output = '<select name="off_redirect" id="off_redirect"><option value="">Active Site</option>';

		foreach ($blog_list AS $blog) {
			// determine whether do display a selected site
			$selected = '';
			if ( $blog['blog_id'] == $off_redirect ) $selected = 'selected';
			// don't offer this site as an option
			if ($blog['blog_id'] != $this_blog_id) {
				
				$blog_url = $blog['domain'].$blog['path'];
				$output .= '<option value="'.$blog['blog_id'].'" '.$selected.'>'.$blog_url.'</option>';
			}
		}
		// display it
		echo $output.'</select>';

	}

}
add_action( 'admin_init', 'off_redirect_settings_api_init' );


// redirect helper function
// returns composed blog url based on redirect settings
function off_get_redirect_url() {
	
	$blog_url = '';
	
	if ( get_option( 'off_redirect', false ) ) {

		$blog_id = get_option('off_redirect', false );
		
		$blog_details = get_blog_details($blog_id);

		$blog_url = 'http://'.$blog_details->domain.$blog_details->path;

	}

	return $blog_url;

}


?>