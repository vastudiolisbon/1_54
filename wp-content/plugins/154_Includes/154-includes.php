<?php

/**
 * @package 1:54 Includes
 * @version 1.0
 */

/*

Plugin Name: 1:54 Includes
Description: Custom post types, taxonomies, settings and functionality for 1:54
Author: TH
Version: 1.0

*/

/* Custom Post Types
-------------------------------------------------------------- */

// get blog details
$this_blog = get_blog_details();

// don't load custom post types for the root site
if ( $this_blog->path != '/' ) :
	
	// Ads
	include_once 'posttype_off_adverts.php';
	// Artists
	include_once 'posttype_off_artist.php';
	// Exhibitors
	include_once 'posttype_off_exhibitor.php';

	// Custom post type admin columns
	add_action("manage_posts_custom_column", "custom_columns");
	function custom_columns($column) {
		global $post;
		switch ($column) {
			case "thumbnail":
				if(function_exists('the_post_thumbnail')) {
					echo the_post_thumbnail(array(60,60,true));
				} else { echo 'NA'; }
				break;
			case "edition":
				echo get_the_term_list($post->ID,'edition','',', ','');
				break;
		}
	}

endif;


/* Edition Taxonomy
-------------------------------------------------------------- */
include_once 'taxonomy_off_edition.php';


/* Settings
-------------------------------------------------------------- */
include_once 'admin_off_settings.php';


/* Custom Fields
-------------------------------------------------------------- */

// customize ACF path
add_filter('acf/settings/path', 'off_acf_settings_path');
function off_acf_settings_path( $path ) {
	// update path
	$path = plugin_dir_path(__FILE__) . '/acf/';
	// return
	return $path;
}

// customize ACF directory
add_filter('acf/settings/dir', 'off_acf_settings_dir');
function off_acf_settings_dir( $dir ) {
	// update path
	$dir = plugin_dir_url(__FILE__) . '/acf/';
	// return
	return $dir;
}

// hide ACF field group menu items
add_filter('acf/settings/show_admin', '__return_false');

// include ACF
include_once( plugin_dir_path(__FILE__) . '/acf/acf.php' );

// include custom field settings
include_once 'admin_off_custom-fields.php';


?>