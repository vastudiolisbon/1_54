<?php

/**
* @package 1:54 Fair Editions
* @version 1.0
* @author Adapted from Periodical Publishing by [Chris Knowles](http://wpmu.org),
* Adds functionality for publishing edition-based / periodical content
*/

// Register the editions Custom Taxonomy
// query_var => true provides automatic filtering of posts if edition in querystring
function off_custom_taxonomy_edition() {

	$labels = array(
		'name'                       => _x( 'Editions', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Edition', 'Taxonomy Singular Name', 'text_domain'),
		'menu_name'                  => __( 'Editions', 'text_domain' ),
		'all_items'                  => __( 'All editions', 'text_domain' ),
		'parent_item'                => __( '', 'text_domain' ),
		'parent_item_colon'          => __( '', 'text_domain' ),
		'new_item_name'              => __( 'Add New edition', 'text_domain' ),
		'edit_item'                  => __( 'Edit edition', 'text_domain' ),
		'update_item'                => __( 'Update edition', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate editions with commas', 'text_domain' ),
		'search_items'               => __( 'Search editions', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove editions', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used editions', 'text_domain' )
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'snow_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'query_var'                  => true,
		'has_archive'                => true,
		'rewrite'                    => array(
			'slug' => 'edition',
			'with_front' => true,
			'pages' => false
			)
	);
	register_taxonomy( 'edition', array('page','off_artist','off_exhibitor'), $args );

}
// register the edition taxonomy via the init action
add_action( 'init', 'off_custom_taxonomy_edition' );

// Load meta field options
include 'taxonomy_off_edition_color.php';

// admin columns
add_filter( 'manage_edit-edition_columns', 'edition_columns' );
function edition_columns( $edition_columns ) {
	$new_columns = array(
		'cb'          => '<input type="checkbox" />',
		'name'        => __( 'edition' ),
		'color'       => __( 'Color' ),
		'description' => __( 'Desc.' ),
		'slug'        => __( 'Slug' ),
		'posts'       => __( 'Posts' )
		);
	return $new_columns;
}
// build columns
add_filter( 'manage_edition_custom_column', 'manage_edition_columns', 10, 3 );
function manage_edition_columns( $out, $column_name, $edition_id ) {
	$edition = get_term( $edition_id, 'edition' );
	switch ( $column_name ) {
		case 'color':
			// get color
			$term_meta = get_option( "taxonomy_term_$edition_id" );
			$color_background = $term_meta['color_light'];
			$color_text = $term_meta['color_dark'];
			$out .= '<div style="width:55px;height:25px;background-color:'. $color_background .';"></div><div style="width:55px;height:25px;background-color:'. $color_text .';"></div>';
			break;
		default:
			break;
	}
	return $out;
}



/* Helper functions
-------------------------------------------------------------- */

// CURRENT EDITION OBJECT
// get all current edition info (not just the slug)
function off_get_current_edition_object() {
	$off_current_edition_object = get_term_by( 'slug', get_option( 'off_current_edition', false ), 'edition' );
	return $off_current_edition_object;
}

// CURRENT EDITION
function off_get_current_edition() {
	$off_current_edition = get_option( 'off_current_edition', false );
	return $off_current_edition;
}


?>