<?php

/**
* @package 1:54 Artist Post type
* @version 1.0
*/

function off_artists() {

	$labels = array(
		'name'                => _x( 'Artists', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Artist', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Artists', 'text_domain' ),
		'parent_item_colon'   => __( '', 'text_domain' ),
		'all_items'           => __( 'Artists', 'text_domain' ),
		'view_item'           => __( 'View artist', 'text_domain' ),
		'add_new_item'        => __( 'Add new artist', 'text_domain' ),
		'add_new'             => __( 'Add an artist', 'text_domain' ),
		'edit_item'           => __( 'Edit artist', 'text_domain' ),
		'update_item'         => __( 'Update artist', 'text_domain' ),
		'search_items'        => __( 'Search artist', 'text_domain' ),
		'not_found'           => __( 'No artists here', 'text_domain' ),
		'not_found_in_trash'  => __( 'No artists found in trash', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'Artists', 'text_domain' ),
		'description'         => __( 'Artists', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title','editor','thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'             => array(
			'slug' => 'artists', 
			'with_front' => false,
			'pages' => false
			),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'
	);
	register_post_type( 'off_artist', $args );

}
// Hook into the 'init' action
add_action( 'init', 'off_artists', 0 );

// admin options
add_filter("manage_edit-off_artist_columns", "off_artist_edit_columns");
function off_artist_edit_columns($columns){  
	$columns = array(  
		"cb" => "<input type=\"checkbox\" />",  
		"thumbnail" => "Image",
		"title" => "Artist",
		"edition" => "Edition",
		"date" => "Date",
	);
	return $columns;  
}

?>