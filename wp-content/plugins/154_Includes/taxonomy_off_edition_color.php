<?php
// editions taxonomy
// custom fields
// http://sabramedia.com/blog/how-to-add-custom-fields-to-custom-taxonomies


// callback function to add a color field to the editions taxonomy
function edition_taxonomy_custom_fields( $edition ) {
   // Check for existing taxonomy meta for the term you're editing
    $edition_id = $edition->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$edition_id" ); // Do the check
?>

<tr class="form-field">
	<th scope="row" valign="top">
		<label for="color_light"><?php _e('Theme Color (Light)'); ?></label>
	</th>
	<td>
		<input type="color" name="term_meta[color_light]" id="term_meta[color_light]" value="<?php echo $term_meta['color_light'] ? $term_meta['color_light'] : '#999999'; ?>" style="height:75px;width:95%;"><br />
		<p class="description"><?php _e('Click the field above to set this edition\'s primary, or light, color.'); ?></p>
	</td>
</tr>
<tr class="form-field">
    <th scope="row" valign="top">
        <label for="color_dark"><?php _e('Theme Color (Dark)'); ?></label>
    </th>
    <td>
        <input type="color" name="term_meta[color_dark]" id="term_meta[color_dark]" value="<?php echo $term_meta['color_dark'] ? $term_meta['color_dark'] : '#303030'; ?>" style="height:75px;width:95%;"><br />
        <p class="description"><?php _e('Click the field above to set this edition\'s secondary, or dark, color. If there is no secondary color, leave this field blank.'); ?></p>
    </td>
</tr>

<?php
}

// callback function to save the color fields
function save_taxonomy_custom_fields( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    }
}

// Add the custom field to the 'editions' taxonomy
add_action( 'edition_edit_form_fields', 'edition_taxonomy_custom_fields', 10, 2 );

// Save the changes made on the "presenters" taxonomy, using our callback function
add_action( 'edited_edition', 'save_taxonomy_custom_fields', 10, 2 );