<?php

/**
* @package 1:54 exhibitor Post type
* @version 1.0
*/

function off_exhibitors() {

	$labels = array(
		'name'                => _x( 'Exhibitors', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Exhibitor', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Exhibitors', 'text_domain' ),
		'parent_item_colon'   => __( '', 'text_domain' ),
		'all_items'           => __( 'Exhibitors', 'text_domain' ),
		'view_item'           => __( 'View exhibitor', 'text_domain' ),
		'add_new_item'        => __( 'Add new exhibitor', 'text_domain' ),
		'add_new'             => __( 'Add an exhibitor', 'text_domain' ),
		'edit_item'           => __( 'Edit exhibitor', 'text_domain' ),
		'update_item'         => __( 'Update exhibitor', 'text_domain' ),
		'search_items'        => __( 'Search exhibitor', 'text_domain' ),
		'not_found'           => __( 'No exhibitors here', 'text_domain' ),
		'not_found_in_trash'  => __( 'No exhibitors found in trash', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'Exhibitor', 'text_domain' ),
		'description'         => __( 'Exhibitor', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title','editor','thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'rewrite'             => array(
			'slug' => 'exhibitors', 
			'with_front' => false,
			'pages' => false
			),
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post'
	);
	register_post_type( 'off_exhibitor', $args );

}
// Hook into the 'init' action
add_action( 'init', 'off_exhibitors', 0 );

// admin options
add_filter("manage_edit-off_exhibitor_columns", "off_exhibitor_edit_columns");

function off_exhibitor_edit_columns($columns){  
	$columns = array(  
		"cb" => "<input type=\"checkbox\" />",  
		"thumbnail" => "Image",
		"title" => "Exhibitor",
		"edition" => "Edition",
		"date" => "Date"
	);
	return $columns;  
}

?>