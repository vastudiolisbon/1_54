<?php

/**
* @package Advertisement Post type
* @version 1.0
*/

function off_custom_adverts() {

	$labels = array(
		'name'                => _x( 'Advert', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Advertisement', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Adverts', 'text_domain' ),
		'parent_item_colon'   => __( '', 'text_domain' ),
		'all_items'           => __( 'Advertisements', 'text_domain' ),
		'view_item'           => __( 'View Ad', 'text_domain' ),
		'add_new_item'        => __( 'Add a new advertisement', 'text_domain' ),
		'add_new'             => __( 'Create a new ad', 'text_domain' ),
		'edit_item'           => __( 'Edit Ad', 'text_domain' ),
		'update_item'         => __( 'Update Ad', 'text_domain' ),
		'search_items'        => __( 'Search Ads', 'text_domain' ),
		'not_found'           => __( 'No advertisements here', 'text_domain' ),
		'not_found_in_trash'  => __( 'No advertisements found in trash', 'text_domain' )
	);
	$args = array(
		'label'               => __( 'Advertisements', 'text_domain' ),
		'description'         => __( 'Advertisements', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title','thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'post'
	);
	register_post_type( 'off_adverts', $args );

}
// Hook into the 'init' action
// add_action( 'init', 'off_custom_adverts', 0 );

// admin options
add_filter("manage_edit-off_adverts_columns", "off_adverts_edit_columns");
function off_adverts_edit_columns($off_adverts_columns){  
	$off_adverts_columns = array(  
		"cb" => "<input type=\"checkbox\" />", 
		"thumbnail" => "Image",
		"title" => "Ads",
		"date" => "Date"
	);
	return $off_adverts_columns;  
}
?>