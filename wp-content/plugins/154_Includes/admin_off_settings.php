<?php

/**
 * 1:54 Settings
 * @package 1:54 Includes
 * @version 1.0
 */


/* Graphics
-------------------------------------------------------------- */

function off_graphics_settings_api_init() {
	add_settings_section( 
			'off_graphics_settings_section',
			'Location Graphics',
			'off_graphics_settings_section_function',
			'general'
		);

	// light bg
	add_settings_field( 
			'off_graphics_logo_light_bg',
			'1:54 Logo (Light Background)',
			'off_graphics_logo_light_bg_function',
			'general',
			'off_graphics_settings_section',
			array(
				'off_graphics_logo_light_bg',
				'off_graphics_logo_light_bg_hover'
			)
		);

	// dark bg
	add_settings_field( 
			'off_graphics_logo_dark_bg',
			'1:54 Logo (Dark Background)',
			'off_graphics_logo_dark_bg_function',
			'general',
			'off_graphics_settings_section',
			array(
				'off_graphics_logo_dark_bg',
				'off_graphics_logo_dark_bg_hover'
			)
		);

	// forum bg
	add_settings_field( 
			'off_graphics_forum_bg',
			'FORUM Background',
			'off_graphics_forum_bg_function',
			'general',
			'off_graphics_settings_section',
			array(
				'off_graphics_forum_bg',
				'off_graphics_forum_bg_2x'
			)
		);

	// favicon
	add_settings_field( 
			'off_graphics_favicon',
			'Favicon (Site Icon)',
			'off_graphics_favicon_function',
			'general',
			'off_graphics_settings_section'
		);

	register_setting( 'general', 'off_graphics_logo_light_bg' );
	register_setting( 'general', 'off_graphics_logo_light_bg_hover' );
	register_setting( 'general', 'off_graphics_logo_dark_bg' );
	register_setting( 'general', 'off_graphics_logo_dark_bg_hover' );
	register_setting( 'general', 'off_graphics_forum_bg' );
	register_setting( 'general', 'off_graphics_forum_bg_2x' );
	register_setting( 'general', 'off_graphics_favicon' );

	// section description
	function off_graphics_settings_section_function() {
		
		echo '<p>Location graphics are the graphics which pertain specifically to <a href="'.esc_url( home_url( '/' ) ).'" target="_blank">'.get_site_url().'</a>. The location graphics should be uploaded via the <a href="'.get_admin_url(get_current_blog_id(), '/').'upload.php" target="_blank">Media Library</a>.</p><p>To add or change the location graphics, find the uploaded image on the Media page and paste the image URL into the text fields below. Refer to the <a href="https://docs.google.com/document/d/1onr3qbPRGxiqRcdvgS97kezY7F8Y1-p2qURxq4d2oW0/" target="_blank">documentation</a> for guidelines and more information on how to create the location graphics for this site.</p>';

		echo '<p>Graphics currently in use:</p>';

		?>

		<p>
			<strong>&#9679; Favicon</strong>
			<br>
			<br>
			<img style="width:16px;" src="<?php echo get_option('off_graphics_favicon', get_template_directory_uri().'/assets/images/default/154_graphics_default_favicon-32.png'); ?>">
		</p>
		<p>
			<strong>&#9679; Light Background: Static &amp; Hovered</strong>
			<br>
			<br>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_logo_light_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_light-bg.png'); ?>">
			<span>&nbsp;</span>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_logo_light_bg_hover', get_template_directory_uri().'/assets/images/default/154_graphics_default_light-bg_hover.png'); ?>">
			<br>
			<br>
		</p>
		<p>
			<strong>&#9679; Dark Background: Static &amp; Hovered</strong>
			<br>
			<br>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_logo_dark_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_dark-bg.png'); ?>">
			<span>&nbsp;</span>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_logo_dark_bg_hover', get_template_directory_uri().'/assets/images/default/154_graphics_default_dark-bg_hover.png'); ?>">
			<br>
			<br>
		</p>
		<p>
			<strong>&#9679; Forum Background: 1x Scale &amp; 2x Scale</strong>
			<br>
			<br>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_forum_bg', get_template_directory_uri().'/assets/images/default/154_graphics_default_forum-bg.png'); ?>">
			<span>&nbsp;</span>
			<img style="max-width:150px;" src="<?php echo get_option('off_graphics_forum_bg_2x', get_template_directory_uri().'/assets/images/default/154_graphics_default_forum-bg_2x.png'); ?>">
			<br>
			<br>
		</p>

		<?php
	}

	// light bg
	function off_graphics_logo_light_bg_function() {
		echo '<p><strong>Static Logo</strong></p>';
		echo '<input name="off_graphics_logo_light_bg" type="text" id="off_graphics_logo_light_bg" value="'.get_option('off_graphics_logo_light_bg', false).'" class="regular-text ltr" placeholder="Image URL">';

		echo '<p><strong>Hovered Logo</strong></p>';
		echo '<input name="off_graphics_logo_light_bg_hover" type="text" id="off_graphics_logo_light_bg_hover" value="'.get_option('off_graphics_logo_light_bg_hover', false).'" class="regular-text ltr" placeholder="Image URL">';
		echo '<p class="description">This is the version of the logo which will appear when users hover over the logo in the header. <strong>The hovered logo should be the exact same size as the static logo.</strong></p>';
	}

	// dark bg
	function off_graphics_logo_dark_bg_function() {
		echo '<p><strong>Static Logo</strong></p>';
		echo '<input name="off_graphics_logo_dark_bg" type="text" id="off_graphics_logo_dark_bg" value="'.get_option('off_graphics_logo_dark_bg', false).'" class="regular-text ltr" placeholder="Image URL">';

		echo '<p><strong>Hovered Logo</strong></p>';
		echo '<input name="off_graphics_logo_dark_bg_hover" type="text" id="off_graphics_logo_dark_bg_hover" value="'.get_option('off_graphics_logo_dark_bg_hover', false).'" class="regular-text ltr" placeholder="Image URL">';
		echo '<p class="description">This is the version of the logo which will appear when users hover over the logo in the header. <strong>The hovered logo should be the exact same size as the static logo.</strong></p>';
	}

	// forum
	function off_graphics_forum_bg_function() {

		echo '<p><strong>1x Scale</strong></p>';
		echo '<input name="off_graphics_forum_bg" type="text" id="off_graphics_forum_bg" value="'.get_option('off_graphics_forum_bg', false).'" class="regular-text ltr" placeholder="Image URL">';

		echo '<p><strong>2x Scale (Retina screens)</strong></p>';
		echo '<input name="off_graphics_forum_bg_2x" type="text" id="off_graphics_forum_bg_2x" value="'.get_option('off_graphics_forum_bg_2x', false).'" class="regular-text ltr" placeholder="Image URL">';
		echo '<p class="description">This image will be displayed on retina and other screens with a higher pixel density than 72ppi. <strong>The 2x scale image should be the same proportions and design as the 1x scale image, but double the size.</strong></p>';
	}

	// favicon
	function off_graphics_favicon_function() {

		echo '<input name="off_graphics_favicon" type="text" id="off_graphics_favicon" value="'.get_option('off_graphics_favicon', false).'" class="regular-text ltr" placeholder="Image URL">';
		echo '<p class="description">This image will be displayed in the tab or URL field at the top of the browser window. <strong>The favicon image should be 32 x 32px square, <em>PNG format only</em>.</strong></p>';
		echo '<p><br><img src="'.plugin_dir_url( __FILE__ ).'/images/154_graphics-settings_00.png" style="zoom:0.5;"></p>';
	}


}
add_action( 'admin_init', 'off_graphics_settings_api_init' );


/* Location Prefix
-------------------------------------------------------------- */

function off_location_prefix_api_init() {
	// add location prefix settings section
	add_settings_section( 
			'off_location_prefix_settings_section',
			'Location Settings',
			'off_location_prefix_settings_section_function',
			'general'
		);
	// location prefix
	add_settings_field( 
			'off_location_prefix',
			'Location Prefix',
			'off_location_prefix_function',
			'general',
			'off_location_prefix_settings_section'
		);

	register_setting( 'general', 'off_location_prefix' );

	// section description
	function off_location_prefix_settings_section_function() {
		echo '<p>Set the location prefix for <a href="'.esc_url( home_url( '/' ) ).'" target="_blank">'.get_site_url().'</a>. The location prefix is used to link to all the necessary assets created for and used specifically by <a href="'.esc_url( home_url( '/' ) ).'" target="_blank">'.get_site_url().'</a>. <strong>Once set, the location prefix should not be changed.</strong></p>';
	}
	// location prefix
	function off_location_prefix_function() {		
		echo '<p><input type="text" class="regular-text" name="off_location_prefix" placeholder="london" value="'.get_option('off_location_prefix').'" /></p>';

		echo '<p class="description">The location prefix should only contain lowercase letters a-z. It should <strong>not</strong> contain any punctuation, special characters, spaces, or numbers.</p>';
	}

}
add_action( 'admin_init', 'off_location_prefix_api_init' );


/* Current Edition
-------------------------------------------------------------- */

function off_edition_settings_api_init() {
	// add edition settings section
	add_settings_section( 
			'off_edition_settings_section',
			'Edition Settings',
			'off_edition_settings_section_function',
			'general'
		);

	// current edition
	add_settings_field( 
			'off_current_edition',
			'Current Edition',
			'off_current_edition_settings_function',
			'general',
			'off_edition_settings_section'
		);

	// menu color
	add_settings_field( 
			'off_menu_color',
			'Menu Color',
			'off_menu_color_function',
			'general',
			'off_edition_settings_section'
		);

	register_setting( 'general', 'off_current_edition' );
	register_setting( 'general', 'off_menu_color' );

	// section description
	function off_edition_settings_section_function() {
		echo '<p>Set the current fair edition. Note that changing the current fair edition will archive all content tagged with the previous fair edition year.</p>';
	}

	// off_current_edition dropdown
	function off_current_edition_settings_function() {
		// get all editions
		$terms = get_terms( 'edition' );
		$off_current_edition = get_option( 'off_current_edition', '' );
		// build the select
		$output = '<select name="off_current_edition" id="off_current_edition"><option value="">Current Edition</option>';
		// build the options
		foreach ( $terms as $term ) {
			$selected = '';
			if ( $term->slug == $off_current_edition ) $selected = ' selected';

			$output .= '<option value="' . $term->slug . '"' . $selected . '>' . $term->name . '</option>';
		}
		// display it
		echo $output . '</select>';
	}

	// menu color settings
	function off_menu_color_function() {
		
		echo '<p class="description">Set the top navigation menu background color scheme. This will also affect the home page background color.</p>';

		$checked = '';
		if ( get_option('off_menu_color') == 'white-bg' ){ $checked = 'checked="checked"'; }

		echo '<p><fieldset>';
		echo '<label><input type="radio" name="off_menu_color" value="white-bg" '.$checked.'/><span>White</span></label>';
		echo '<br>';

		$checked = '';
		if ( get_option('off_menu_color') == 'dark-bg' ){ $checked = 'checked="checked"'; }

		echo '<label><input type="radio" name="off_menu_color" value="dark-bg" '.$checked.'/><span>Current fair edition dark background color</span></label></fieldset></p>';

	}

}
add_action( 'admin_init', 'off_edition_settings_api_init' );


/* Location Links (header)
-------------------------------------------------------------- */

function off_location_links_settings_api_init() {

	// location links section
	add_settings_section( 
			'off_location_links_section',
			'Location Links',
			'off_location_links_section_function',
			'general'
		);

	// ^^^^^^^^^^^^^^
	// first location
	add_settings_field( 
			'off_location_link1',
			'Home Location',
			'off_location_link1_function',
			'general',
			'off_location_links_section',
			array (
				'off_location_link1_date_open',
				'off_location_link1_date_close',	
				'off_location_link1_month_open',
				'off_location_link1_month_close',
				'off_location_link1_year'
			)
		);

	register_setting( 'general', 'off_location_link1' );
	register_setting( 'general', 'off_location_link1_date_open' );
	register_setting( 'general', 'off_location_link1_date_close' );
	register_setting( 'general', 'off_location_link1_month_open' );
	register_setting( 'general', 'off_location_link1_month_close' );
	register_setting( 'general', 'off_location_link1_year' );



	// ^^^^^^^^^^^^^^^
	// second location
	add_settings_field( 
			'off_location_link2',
			'Second Location',
			'off_location_link2_function',
			'general',
			'off_location_links_section',
			array (
				'off_location_link2_date_open',
				'off_location_link2_date_close',	
				'off_location_link2_month_open',
				'off_location_link2_month_close',
				'off_location_link2_year'
			)
		);
	
	register_setting( 'general', 'off_location_link2' );
	register_setting( 'general', 'off_location_link2_url' );
	register_setting( 'general', 'off_location_link2_date_open' );
	register_setting( 'general', 'off_location_link2_date_close' );
	register_setting( 'general', 'off_location_link2_month_open' );
	register_setting( 'general', 'off_location_link2_month_close' );
	register_setting( 'general', 'off_location_link2_year' );

	// section description
	function off_location_links_section_function() {
		echo '<p>Set the text and URL for two 1:54 locations. This will display in the header on every page. Note:</p><ul><li>&#9679; The first 1:54 location for this site will always be <a href="'.esc_url( home_url( '/' ) ).'" target="_blank">'.get_site_url().'</a></li><li>&#9679; The edition year will be determined by the current edition (indicated above under <em>Edition Settings</em>), unless noted otherwise, below.</li></ul>';
	}

	function list_months() {
		return '<option value="January">January</option><option value="February">February</option><option value="March">March</option><option value="April">April</option><option value="May">May</option><option value="June">June</option><option value="July">July</option><option value="August">August</option><option value="September">September</option><option value="October">October</option><option value="November">November</option><option value="December">December</option>';
	}

	// FIRST LOCATION (this site)
	function off_location_link1_function($args) {
		
		// city name
		echo '<p><strong>Location City</strong></p>';
		echo '<p><input name="off_location_link1" type="text" maxlength="15" id="off_location_link1" value="'.get_option('off_location_link1', false).'" class="regular-text ltr" placeholder="City"></p>';
		
		// URL
		echo '<p><strong>Location Site URL</strong></p>';
		echo '<p><code>'.get_site_url().'</code></p>';

		// opening dates
		echo '<p><strong>Opening dates</strong></p>';		
		echo '<p>';
		
		// month
		echo '<select name="off_location_link1_month_open" id="off_location_link1_month_open" onchange="" size="1"><option value="'.get_option('off_location_link1_month_open', false ).'" selected>'.get_option('off_location_link1_month_open', 'Opening Month').'</option>'.list_months().'</select> ';
		
		// day
		echo '<input name="off_location_link1_date_open" type="number" min="1" max="31" id="off_location_link1_date_open" value="'.get_option('off_location_link1_date_open', false).'" class="ltr">';
		
		echo '</p>';

		// closing dates
		echo '<p><strong>Closing dates</strong></p>';
		echo '<p class="description">Enter the day of the month. Ignore the month drop-down field if the closing month is the same as the opening month.</p>';
		echo '<p>';
		
		// month
		echo '<select name="off_location_link1_month_close" id="off_location_link1_month_close" onchange="" size="1"><option value="'.get_option('off_location_link1_month_close', 'same').'" selected>'.ucfirst(get_option('off_location_link1_month_close', 'Same as opening month')).'</option>'.list_months().'</select> ';

		// day
		echo '<input name="off_location_link1_date_close" type="number" min="1" max="31" id="off_location_link1_date_close" value="'.get_option('off_location_link1_date_close', false).'" class="ltr">';

		echo '</p>';

		// edition year
		// will default to current edition setting
		echo '<p><strong>Edition Year</strong></p>';
		echo '<p class="description">By default, this will pick up the current edition, set above. Change that value here if you want to display a different year in the header location links.</p>';
		echo '<p><input name="off_location_link1_year" type="number" min="2010" max="2050" id="off_location_link1_year" value="'.get_option('off_location_link1_year', off_get_current_edition()).'" class="regular-text ltr" placeholder="'.off_get_current_edition().'"></p>';

	}


	// SECOND LOCATION (another site on the network)
	function off_location_link2_function() {
		// city name
		echo '<p><strong>Location City</strong></p>';
		echo '<p><input name="off_location_link2" type="text" maxlength="15" id="off_location_link2" value="'.get_option('off_location_link2', false).'" class="regular-text ltr"></p>';
		// URL
		echo '<p><strong>Location Site URL</strong></p>';
		// query all the sites on the network
		$args = array(
				'network_id' => get_current_site()->id,
				'public' => true,
				'deleted' => false,
				'offset' => 0
			);
		// get the full site list
		$blog_list = wp_get_sites($args);
		// get this site's blog id
		$this_blog_id = get_current_blog_id();
		// has a selection already been made?
		$off_location_link2_url = get_option('off_location_link2_url','');

		$output = '<select name="off_location_link2_url" id="off_location_link2_url"><option value="">Choose a site</option>';

		foreach ($blog_list AS $blog) {
			// determine whether do display a selected site
			$selected = '';
			if ( $blog['blog_id'] == $off_location_link2_url ) $selected = 'selected';
			// don't offer this site as an option, and don't allow linking to the root site
			if ( ($blog['blog_id'] != $this_blog_id) && ($blog['path'] != '/' ) ) {
				$blog_url = $blog['domain'].$blog['path'];
				$output .= '<option value="'.$blog['blog_id'].'" '.$selected.'>'.$blog_url.'</option>';
			}
		}
		// display it
		echo $output.'</select>';

		// opening dates
		echo '<p><strong>Opening dates</strong></p>';
		echo '<p>';

		echo '<select name="off_location_link2_month_open" id="off_location_link2_month_open" onchange="" size="1"><option value="'.get_option('off_location_link2_month_open', false).'" selected>'.ucfirst(get_option('off_location_link2_month_open', 'Opening Month') ).'</option>'.list_months().'</select> ';

		echo '<input name="off_location_link2_date_open" type="number" min="1" max="31" id="off_location_link2_date_open" value="'.get_option('off_location_link2_date_open', false).'" class="ltr">';

		echo '</p>';

		// closing dates
		echo '<p><strong>Closing dates</strong></p>';
		echo '<p class="description">Enter the day of the month. Ignore the month drop-down field if the closing month is the same as the opening month.</p>';
		echo '<p>';

		echo '<select name="off_location_link2_month_close" id="off_location_link2_month_close" onchange="" size="1"><option value="'.get_option('off_location_link2_month_close', 'same' ).'" selected>'.ucfirst( get_option('off_location_link2_month_close', 'Same as opening month')).'</option>'.list_months().'</select> ';

		echo '<input name="off_location_link2_date_close" type="number" min="1" max="31" id="off_location_link2_date_close" value="'.get_option('off_location_link2_date_close', false).'" class="ltr">';

		echo '</p>';

		// edition year
		// will default to current edition setting
		echo '<p><strong>Edition Year</strong></p>';
		echo '<p class="description">By default, this will pick up the current edition, set above. Change that value here if you want to display a different year in the header location links.</p>';
		echo '<p><input name="off_location_link2_year" type="number" min="2010" max="2050" id="off_location_link2_year" value="'.get_option('off_location_link2_year', off_get_current_edition()).'" class="regular-text ltr" placeholder="'.off_get_current_edition().'"></p>';

	}

}
add_action( 'admin_init', 'off_location_links_settings_api_init' );


// location URL helper function
// returns composed blog url based on location link settings
function off_get_location_url() {
	
	$blog_url = '';
	
	if ( get_option( 'off_location_link2_url', false ) ) {

		$blog_id = get_option( 'off_location_link2_url', false );

		if ( is_int((int)$blog_id) ) {

			$blog_details = get_blog_details($blog_id);
			$blog_url = 'http://'.$blog_details->domain.$blog_details->path;

		}

	}

	return $blog_url;

}


/* Address & Contact (footer)
-------------------------------------------------------------- */

function off_address_settings_api_init() {

	// address section
	add_settings_section(
			'off_footer_settings_section',
			'Address & Contact Information',
			'off_footer_settings_section_function',
			'general'
		);

	// email
	add_settings_field(
			'off_contact_email',
			'Contact',
			'off_contact_function',
			'general',
			'off_footer_settings_section',
			array(
				'off_contact_phone'
			)
		);

	register_setting( 'general', 'off_contact_email' );
	register_setting( 'general', 'off_contact_phone' );

	// address line 1
	add_settings_field(
			'off_contact_address_line1',
			'Address',
			'off_contact_address_function',
			'general',
			'off_footer_settings_section',
			array(
				'off_contact_address_line2',
				'off_contact_address_line3',
				'off_contact_address_line4'
			)
		);

	register_setting( 'general', 'off_contact_address_line1' );
	register_setting( 'general', 'off_contact_address_line2' );
	register_setting( 'general', 'off_contact_address_line3' );
	register_setting( 'general', 'off_contact_address_line4' );

	// intro message for the footer settings section
	function off_footer_settings_section_function() {
		echo '<p>Add contact information for this location. This information will be displayed in the footer on all pages.</p>';
	}

	// contact info
	function off_contact_function() {
		echo '<p><strong>Email</strong></p>';
		echo '<p class="description">If your contact email is not displaying on the site, double-check your entry to be sure you\'re using a valid email address.</p>';
		echo '<p><input name="off_contact_email" type="email" id="off_contact_email" value="'.get_option('off_contact_email', false).'" class="regular-text ltr"></p>';
		
		echo '<p><strong>Telephone</strong></p>';
		echo '<input name="off_contact_phone" type="tel" id="off_contact_phone" value="'.get_option('off_contact_phone', false).'" class="regular-text ltr">';

	}

	// address
	function off_contact_address_function() {

		echo '<p>Address line 1</p>';
		echo '<input name="off_contact_address_line1" type="text" id="off_contact_address_line1" value="'.get_option('off_contact_address_line1', false).'" class="regular-text ltr" placeholder="Art Africa Ltd">';
		echo '<p>Address line 2</p>';
		echo '<input name="off_contact_address_line2" type="text" id="off_contact_address_line2" value="'.get_option('off_contact_address_line2', false).'" class="regular-text ltr" placeholder="Somerset House">';
		echo '<p>City</p>';
		echo '<input name="off_contact_address_line3" type="text" id="off_contact_address_line3" value="'.get_option('off_contact_address_line3', false).'" class="regular-text ltr" placeholder="Strand, London WC2R 1LA">';
		echo '<p>Country</p>';
		echo '<input name="off_contact_address_line4" type="text" id="off_contact_address_line4" value="'.get_option('off_contact_address_line4', false).'" class="regular-text ltr" placeholder="United Kingdom">';
	}

}
add_action( 'admin_init', 'off_address_settings_api_init' );


?>